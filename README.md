# Voxer #

Ce programme est un clone du célèbre Minecraft, facile à recréer car ne nécessitant pas de compétences artistiques.

### But du projet ###

Ce projet nous a permis d'expérimenter avec OpenGL et d'apprendre les techniques de rendu 3D, ainsi que de progresser dans le langage Java. Il a été réalisé dans le cadre de la matière Informatique et Sciences du Numérique en Terminale et a été présenté au BAC.

### Screenshots ###

![minecraft-clone.jpg](https://bitbucket.org/repo/8Mn5BG/images/821694227-minecraft-clone.jpg)

### Exécutable précompilé ###

Pour Windows (necessite Java) : https://bitbucket.org/Toun/voxer/downloads/Executable.rar

### Contributeurs ###

* Tristan Daniel : moteur graphique
* Maël Monnier : algorithme de génération de terrain