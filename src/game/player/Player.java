package game.player;

import util.math.Vec2i;
import util.math.Vec3f;

public class Player
{
	public static Vec3f position = new Vec3f(1f, 150f, 1f);
	private static Vec3f rotation = new Vec3f(0.0f, 0.0f, 0.0f);
	private static Vec2i chunk = new Vec2i(0, 0);
	
	public static void update(float delta)
	{
		PlayerController.update(delta);
		PlayerCamera.update();
	}
	
	public static void move(float x, float y, float z)
	{
		position.x += x;
		position.y += y;
		position.z += z;
		
		// int chunkX = BlockManager.getChunkCoordXFromBlockCoordX((int) position.x);
		// int chunkZ = BlockManager.getChunkCoordZFromBlockCoordZ((int) position.z);
		
		// if (chunkX != BlockManager.centerChunk.x || chunkZ != BlockManager.centerChunk.z)
		// BlockManager.shiftChunks(chunkX - BlockManager.centerChunk.x, chunkZ - BlockManager.centerChunk.z);
	}
	
	public static void setPitch(float pitch)
	{
		rotation.x = pitch;
	}
	
	public static void setYaw(float yaw)
	{
		rotation.y = yaw;
	}
	
	public static float getPitch()
	{
		return rotation.x;
	}
	
	public static float getYaw()
	{
		return rotation.y;
	}
	
	public static Vec3f getRotation()
	{
		return rotation;
	}
}