package game.player;

import org.lwjgl.opengl.Display;

import util.math.Mat4f;
import util.math.MathHelper;
import util.math.Vec3f;

public class PlayerCamera
{
	public static Mat4f projectionMatrix = new Mat4f();
	public static Mat4f viewMatrix = new Mat4f();
	public static Mat4f viewMatrixInverse = new Mat4f();
	public static Mat4f viewProjectionMatrix = new Mat4f();
	
	private static final float NEAR = 0.1f;
	private static final float FAR = 1024;
	
	static
	{
		setFieldOfView(70f);
		projectionMatrix.m22 = -((FAR + NEAR) / (FAR - NEAR));
		projectionMatrix.m23 = -1;
		projectionMatrix.m32 = -((2 * NEAR * FAR) / (FAR - NEAR));
		projectionMatrix.m33 = 0;
		updateAspectRatio();
	}
	
	public static void setFieldOfView(float fieldOfView)
	{
		projectionMatrix.m11 = MathHelper.coTangent(MathHelper.degreesToRadians(fieldOfView / 2f));
		updateAspectRatio();
	}
	
	public static void updateAspectRatio()
	{
		projectionMatrix.m00 = projectionMatrix.m11 / ((float) Display.getWidth() / (float) Display.getHeight());
	}
	
	public static void update()
	{
		viewMatrix.setIdentity();
		
		viewMatrix.rotate(Player.getPitch(), Vec3f.xAxis);
		viewMatrix.rotate(Player.getYaw(), Vec3f.yAxis);
		viewMatrix.translate(Player.position);
		
		viewMatrixInverse.set(viewMatrix);
		viewMatrixInverse.invert();
		
		Mat4f.multiply(projectionMatrix, viewMatrix, viewProjectionMatrix);
	}
	
	public static Mat4f getViewMatrix()
	{
		return viewMatrix;
	}
}