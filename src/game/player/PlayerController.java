package game.player;

import game.block.Block;
import game.world.BlockManager;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import start.Voxer;

public class PlayerController
{
	private static final float speed = 10f;
	private static final float mouseSensibility = 0.003f;
	
	private static int keyLeft = Keyboard.KEY_Q;
	private static int keyRight = Keyboard.KEY_D;
	private static int keyForward = Keyboard.KEY_Z;
	private static int keyBack = Keyboard.KEY_S;
	private static int keyUp = Keyboard.KEY_SPACE;
	private static int keyDown = Keyboard.KEY_LSHIFT;
	
	private static int id = Block.getBlockIdFromName("leaves_oak");
	
	public static void update(float delta)
	{
		float pitch = Player.getPitch() - Mouse.getDY() * mouseSensibility;
		if (pitch < -0.5f * Math.PI)
			pitch = (float) (-0.5f * Math.PI);
		else if (pitch > 0.5f * Math.PI)
			pitch = (float) (0.5f * Math.PI);
		
		float yaw = Player.getYaw() + Mouse.getDX() * mouseSensibility;
		if (yaw < -Math.PI)
			yaw += 2 * (float) (Math.PI);
		else if (yaw > Math.PI)
			yaw -= 2 * (float) (Math.PI);
		
		Player.setPitch(pitch);
		Player.setYaw(yaw);
		
		int keyCombination = 0;
		if (Keyboard.isKeyDown(keyForward))
			keyCombination |= 0b1000;
		if (Keyboard.isKeyDown(keyBack))
			keyCombination |= 0b0100;
		if (Keyboard.isKeyDown(keyLeft))
			keyCombination |= 0b0010;
		if (Keyboard.isKeyDown(keyRight))
			keyCombination |= 0b0001;
		
		switch (keyCombination)
		{
			case 0b1000:
				Player.move((float) (Math.sin(yaw) * delta) * speed, 0, -(float) (Math.cos(yaw) * delta) * speed);
				break;
			case 0b0100:
				Player.move(-(float) (Math.sin(yaw) * delta) * speed, 0, (float) (Math.cos(yaw) * delta) * speed);
				break;
			case 0b0010:
				Player.move(-(float) (Math.cos(yaw) * delta) * speed, 0, -(float) (Math.sin(yaw) * delta) * speed);
				break;
			case 0b0001:
				Player.move((float) (Math.cos(yaw) * delta) * speed, 0, (float) (Math.sin(yaw) * delta) * speed);
				break;
			
			case 0b1010:
				Player.move((float) (Math.sin(yaw - Math.PI / 4) * delta) * speed, 0, -(float) (Math.cos(yaw - Math.PI / 4) * delta) * speed);
				break;
			case 0b1001:
				Player.move((float) (Math.sin(yaw + Math.PI / 4) * delta) * speed, 0, -(float) (Math.cos(yaw + Math.PI / 4) * delta) * speed);
				break;
			case 0b0110:
				Player.move(-(float) (Math.sin(yaw + Math.PI / 4) * delta) * speed, 0, (float) (Math.cos(yaw + Math.PI / 4) * delta) * speed);
				break;
			case 0b0101:
				Player.move(-(float) (Math.sin(yaw - Math.PI / 4) * delta) * speed, 0, (float) (Math.cos(yaw - Math.PI / 4) * delta) * speed);
				break;
			
			case 0b1011:
				Player.move((float) (Math.sin(yaw) * delta) * speed, 0, -(float) (Math.cos(yaw) * delta) * speed);
				break;
			case 0b0111:
				Player.move(-(float) (Math.sin(yaw) * delta) * speed, 0, (float) (Math.cos(yaw) * delta) * speed);
				break;
			case 0b1110:
				Player.move(-(float) (Math.cos(yaw) * delta) * speed, 0, -(float) (Math.sin(yaw) * delta) * speed);
				break;
			case 0b1101:
				Player.move((float) (Math.cos(yaw) * delta) * speed, 0, (float) (Math.sin(yaw) * delta) * speed);
				break;
		}
		if (Keyboard.isKeyDown(keyUp))
			Player.move(0, delta * speed, 0);
		if (Keyboard.isKeyDown(keyDown))
			Player.move(0, -delta * speed, 0);
		if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE))
			Voxer.stop();
		
		while (Keyboard.next())
			if (Keyboard.getEventKeyState())
				switch (Keyboard.getEventKey())
				{
					case Keyboard.KEY_0:
						id = Block.getBlockIdFromName("stone");
						break;
					case Keyboard.KEY_1:
						id = Block.getBlockIdFromName("dirt");
						break;
					case Keyboard.KEY_2:
						id = Block.getBlockIdFromName("grass");
						break;
					case Keyboard.KEY_3:
						id = Block.getBlockIdFromName("log_oak");
						break;
					case Keyboard.KEY_4:
						id = Block.getBlockIdFromName("leaves_oak");
						break;
					case Keyboard.KEY_5:
						id = Block.getBlockIdFromName("crafting_table");
						break;
					case Keyboard.KEY_6:
						id = Block.getBlockIdFromName("hardened_clay_stained_cyan");
						break;
					case Keyboard.KEY_7:
						id = Block.getBlockIdFromName("glass");
						break;
					case Keyboard.KEY_8:
						id = Block.getBlockIdFromName("tnt");
						break;
					case Keyboard.KEY_9:
						id = Block.getBlockIdFromName("redstone_lamp_on");
						break;
					case Keyboard.KEY_G:
						BlockManager.updateArtificialLighting();
						break;
					case Keyboard.KEY_H:
						BlockManager.setBlock((int) Player.position.x, (int) Player.position.y, (int) Player.position.z, "redstone_block");
						break;
				}
		
		while (Mouse.next())
			if (Mouse.getEventButtonState() && BlockManager.blockSelected)
				switch (Mouse.getEventButton())
				{
					case 0:
						BlockManager.setBlock(BlockManager.selectedBlock, 0, 0);
						break;
					case 1:
						BlockManager.setBlock(BlockManager.selectedBlockForPlacing, id, 0);
						break;
					case 2:
						id = BlockManager.getBlockId(BlockManager.selectedBlock);
				}
	}
}
