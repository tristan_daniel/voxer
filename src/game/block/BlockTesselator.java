package game.block;

import org.lwjgl.opengl.GL20;

public class BlockTesselator
{
	private static final float one = 1;
	private static final float zero = 0;
	
	private static final float[] topFace = { zero, one, one, 1, 1, one, one, one, 0, 1, one, one, zero, 0, 0, zero, one, zero, 1, 0 };
	private static final float[] frontFace = { zero, zero, one, 1, 1, one, zero, one, 0, 1, one, one, one, 0, 0, zero, one, one, 1, 0 };
	private static final float[] rightFace = { one, zero, one, 1, 1, one, zero, zero, 0, 1, one, one, zero, 0, 0, one, one, one, 1, 0 };
	private static final float[] backFace = { one, zero, zero, 1, 1, zero, zero, zero, 0, 1, zero, one, zero, 0, 0, one, one, zero, 1, 0 };
	private static final float[] leftFace = { zero, zero, zero, 1, 1, zero, zero, one, 0, 1, zero, one, one, 0, 0, zero, one, zero, 1, 0 };
	private static final float[] bottomFace = { zero, zero, zero, 1, 1, one, zero, zero, 0, 1, one, zero, one, 0, 0, zero, zero, one, 1, 0 };
	
	public static void drawYPFace(int x, int y, int z, Block block, float lightLevel)
	{
		int texture = block.getTextureYP();
		
		GL20.glVertexAttrib1f(2, lightLevel);
		GL20.glVertexAttrib3f(1, topFace[3], topFace[4], texture);
		GL20.glVertexAttrib3f(0, topFace[0] + x, topFace[1] + y, topFace[2] + z);
		
		GL20.glVertexAttrib1f(2, lightLevel);
		GL20.glVertexAttrib3f(1, topFace[8], topFace[9], texture);
		GL20.glVertexAttrib3f(0, topFace[5] + x, topFace[6] + y, topFace[7] + z);
		
		GL20.glVertexAttrib1f(2, lightLevel);
		GL20.glVertexAttrib3f(1, topFace[13], topFace[14], texture);
		GL20.glVertexAttrib3f(0, topFace[10] + x, topFace[11] + y, topFace[12] + z);
		
		GL20.glVertexAttrib1f(2, lightLevel);
		GL20.glVertexAttrib3f(1, topFace[18], topFace[19], texture);
		GL20.glVertexAttrib3f(0, topFace[15] + x, topFace[16] + y, topFace[17] + z);
	}
	
	public static void drawZPFace(int x, int y, int z, Block block, float lightLevel)
	{
		int texture = block.getTextureZP();
		
		GL20.glVertexAttrib1f(2, lightLevel);
		GL20.glVertexAttrib3f(1, frontFace[3], frontFace[4], texture);
		GL20.glVertexAttrib3f(0, frontFace[0] + x, frontFace[1] + y, frontFace[2] + z);
		
		GL20.glVertexAttrib1f(2, lightLevel);
		GL20.glVertexAttrib3f(1, frontFace[8], frontFace[9], texture);
		GL20.glVertexAttrib3f(0, frontFace[5] + x, frontFace[6] + y, frontFace[7] + z);
		
		GL20.glVertexAttrib1f(2, lightLevel);
		GL20.glVertexAttrib3f(1, frontFace[13], frontFace[14], texture);
		GL20.glVertexAttrib3f(0, frontFace[10] + x, frontFace[11] + y, frontFace[12] + z);
		
		GL20.glVertexAttrib1f(2, lightLevel);
		GL20.glVertexAttrib3f(1, frontFace[18], frontFace[19], texture);
		GL20.glVertexAttrib3f(0, frontFace[15] + x, frontFace[16] + y, frontFace[17] + z);
	}
	
	public static void drawXPFace(int x, int y, int z, Block block, float lightLevel)
	{
		int texture = block.getTextureXP();
		
		GL20.glVertexAttrib1f(2, lightLevel);
		GL20.glVertexAttrib3f(1, rightFace[3], rightFace[4], texture);
		GL20.glVertexAttrib3f(0, rightFace[0] + x, rightFace[1] + y, rightFace[2] + z);
		
		GL20.glVertexAttrib1f(2, lightLevel);
		GL20.glVertexAttrib3f(1, rightFace[8], rightFace[9], texture);
		GL20.glVertexAttrib3f(0, rightFace[5] + x, rightFace[6] + y, rightFace[7] + z);
		
		GL20.glVertexAttrib1f(2, lightLevel);
		GL20.glVertexAttrib3f(1, rightFace[13], rightFace[14], texture);
		GL20.glVertexAttrib3f(0, rightFace[10] + x, rightFace[11] + y, rightFace[12] + z);
		
		GL20.glVertexAttrib1f(2, lightLevel);
		GL20.glVertexAttrib3f(1, rightFace[18], rightFace[19], texture);
		GL20.glVertexAttrib3f(0, rightFace[15] + x, rightFace[16] + y, rightFace[17] + z);
	}
	
	public static void drawZNFace(int x, int y, int z, Block block, float lightLevel)
	{
		int texture = block.getTextureZN();
		
		GL20.glVertexAttrib1f(2, lightLevel);
		GL20.glVertexAttrib3f(1, backFace[3], backFace[4], texture);
		GL20.glVertexAttrib3f(0, backFace[0] + x, backFace[1] + y, backFace[2] + z);
		
		GL20.glVertexAttrib1f(2, lightLevel);
		GL20.glVertexAttrib3f(1, backFace[8], backFace[9], texture);
		GL20.glVertexAttrib3f(0, backFace[5] + x, backFace[6] + y, backFace[7] + z);
		
		GL20.glVertexAttrib1f(2, lightLevel);
		GL20.glVertexAttrib3f(1, backFace[13], backFace[14], texture);
		GL20.glVertexAttrib3f(0, backFace[10] + x, backFace[11] + y, backFace[12] + z);
		
		GL20.glVertexAttrib1f(2, lightLevel);
		GL20.glVertexAttrib3f(1, backFace[18], backFace[19], texture);
		GL20.glVertexAttrib3f(0, backFace[15] + x, backFace[16] + y, backFace[17] + z);
	}
	
	public static void drawXNFace(int x, int y, int z, Block block, float lightLevel)
	{
		int texture = block.getTextureXN();
		
		GL20.glVertexAttrib1f(2, lightLevel);
		GL20.glVertexAttrib3f(1, leftFace[3], leftFace[4], texture);
		GL20.glVertexAttrib3f(0, leftFace[0] + x, leftFace[1] + y, leftFace[2] + z);
		
		GL20.glVertexAttrib1f(2, lightLevel);
		GL20.glVertexAttrib3f(1, leftFace[8], leftFace[9], texture);
		GL20.glVertexAttrib3f(0, leftFace[5] + x, leftFace[6] + y, leftFace[7] + z);
		
		GL20.glVertexAttrib1f(2, lightLevel);
		GL20.glVertexAttrib3f(1, leftFace[13], leftFace[14], texture);
		GL20.glVertexAttrib3f(0, leftFace[10] + x, leftFace[11] + y, leftFace[12] + z);
		
		GL20.glVertexAttrib1f(2, lightLevel);
		GL20.glVertexAttrib3f(1, leftFace[18], leftFace[19], texture);
		GL20.glVertexAttrib3f(0, leftFace[15] + x, leftFace[16] + y, leftFace[17] + z);
	}
	
	public static void drawYNFace(int x, int y, int z, Block block, float lightLevel)
	{
		int texture = block.getTextureYN();
		
		GL20.glVertexAttrib1f(2, lightLevel);
		GL20.glVertexAttrib3f(1, bottomFace[3], bottomFace[4], texture);
		GL20.glVertexAttrib3f(0, bottomFace[0] + x, bottomFace[1] + y, bottomFace[2] + z);
		
		GL20.glVertexAttrib1f(2, lightLevel);
		GL20.glVertexAttrib3f(1, bottomFace[8], bottomFace[9], texture);
		GL20.glVertexAttrib3f(0, bottomFace[5] + x, bottomFace[6] + y, bottomFace[7] + z);
		
		GL20.glVertexAttrib1f(2, lightLevel);
		GL20.glVertexAttrib3f(1, bottomFace[13], bottomFace[14], texture);
		GL20.glVertexAttrib3f(0, bottomFace[10] + x, bottomFace[11] + y, bottomFace[12] + z);
		
		GL20.glVertexAttrib1f(2, lightLevel);
		GL20.glVertexAttrib3f(1, bottomFace[18], bottomFace[19], texture);
		GL20.glVertexAttrib3f(0, bottomFace[15] + x, bottomFace[16] + y, bottomFace[17] + z);
	}
	
	public static void drawYPFaceWithAmbiantOcclusion(int x, int y, int z, Block block, int YPLightLevel, int ZPLightLevel, int ZPXPLightLevel, int XPLightLevel, int XPZNLightLevel, int ZNLightLevel, int ZNXNLightLevel, int XNLightLevel, int XNZPLightLevel)
	{
		int texture = block.getTextureYP();
		
		GL20.glVertexAttrib1f(2, mixBrightness(YPLightLevel, XNLightLevel, XNZPLightLevel, ZPLightLevel));
		GL20.glVertexAttrib3f(1, topFace[3], topFace[4], texture);
		GL20.glVertexAttrib3f(0, topFace[0] + x, topFace[1] + y, topFace[2] + z);
		
		GL20.glVertexAttrib1f(2, mixBrightness(YPLightLevel, ZPLightLevel, ZPXPLightLevel, XPLightLevel));
		GL20.glVertexAttrib3f(1, topFace[8], topFace[9], texture);
		GL20.glVertexAttrib3f(0, topFace[5] + x, topFace[6] + y, topFace[7] + z);
		
		GL20.glVertexAttrib1f(2, mixBrightness(YPLightLevel, XPLightLevel, XPZNLightLevel, ZNLightLevel));
		GL20.glVertexAttrib3f(1, topFace[13], topFace[14], texture);
		GL20.glVertexAttrib3f(0, topFace[10] + x, topFace[11] + y, topFace[12] + z);
		
		GL20.glVertexAttrib1f(2, mixBrightness(YPLightLevel, ZNLightLevel, ZNXNLightLevel, XNLightLevel));
		GL20.glVertexAttrib3f(1, topFace[18], topFace[19], texture);
		GL20.glVertexAttrib3f(0, topFace[15] + x, topFace[16] + y, topFace[17] + z);
	}
	
	public static void drawZPFaceWithAmbiantOcclusion(int x, int y, int z, Block block, int ZPLightLevel, int YNLightLevel, int YNXPLightLevel, int XPLightLevel, int XPYPLightLevel, int YPLightLevel, int YPXNLightLevel, int XNLightLevel, int XNYNLightLevel)
	{
		int texture = block.getTextureZP();
		
		GL20.glVertexAttrib1f(2, mixBrightness(ZPLightLevel, XNLightLevel, XNYNLightLevel, YNLightLevel));
		GL20.glVertexAttrib3f(1, frontFace[3], frontFace[4], texture);
		GL20.glVertexAttrib3f(0, frontFace[0] + x, frontFace[1] + y, frontFace[2] + z);
		
		GL20.glVertexAttrib1f(2, mixBrightness(ZPLightLevel, YNLightLevel, YNXPLightLevel, XPLightLevel));
		GL20.glVertexAttrib3f(1, frontFace[8], frontFace[9], texture);
		GL20.glVertexAttrib3f(0, frontFace[5] + x, frontFace[6] + y, frontFace[7] + z);
		
		GL20.glVertexAttrib1f(2, mixBrightness(ZPLightLevel, XPLightLevel, XPYPLightLevel, YPLightLevel));
		GL20.glVertexAttrib3f(1, frontFace[13], frontFace[14], texture);
		GL20.glVertexAttrib3f(0, frontFace[10] + x, frontFace[11] + y, frontFace[12] + z);
		
		GL20.glVertexAttrib1f(2, mixBrightness(ZPLightLevel, YPLightLevel, YPXNLightLevel, XNLightLevel));
		GL20.glVertexAttrib3f(1, frontFace[18], frontFace[19], texture);
		GL20.glVertexAttrib3f(0, frontFace[15] + x, frontFace[16] + y, frontFace[17] + z);
	}
	
	public static void drawXPFaceWithAmbiantOcclusion(int x, int y, int z, Block block, int XPLightLevel, int YNLightLevel, int YNZNLightLevel, int ZNLightLevel, int ZNYPLightLevel, int YPLightLevel, int YPZPLightLevel, int ZPLightLevel, int ZPYNLightLevel)
	{
		int texture = block.getTextureXP();
		
		GL20.glVertexAttrib1f(2, mixBrightness(XPLightLevel, ZPLightLevel, ZPYNLightLevel, YNLightLevel));
		GL20.glVertexAttrib3f(1, rightFace[3], rightFace[4], texture);
		GL20.glVertexAttrib3f(0, rightFace[0] + x, rightFace[1] + y, rightFace[2] + z);
		
		GL20.glVertexAttrib1f(2, mixBrightness(XPLightLevel, YNLightLevel, YNZNLightLevel, ZNLightLevel));
		GL20.glVertexAttrib3f(1, rightFace[8], rightFace[9], texture);
		GL20.glVertexAttrib3f(0, rightFace[5] + x, rightFace[6] + y, rightFace[7] + z);
		
		GL20.glVertexAttrib1f(2, mixBrightness(XPLightLevel, ZNLightLevel, ZNYPLightLevel, YPLightLevel));
		GL20.glVertexAttrib3f(1, rightFace[13], rightFace[14], texture);
		GL20.glVertexAttrib3f(0, rightFace[10] + x, rightFace[11] + y, rightFace[12] + z);
		
		GL20.glVertexAttrib1f(2, mixBrightness(XPLightLevel, YPLightLevel, YPZPLightLevel, ZPLightLevel));
		GL20.glVertexAttrib3f(1, rightFace[18], rightFace[19], texture);
		GL20.glVertexAttrib3f(0, rightFace[15] + x, rightFace[16] + y, rightFace[17] + z);
	}
	
	public static void drawZNFaceWithAmbiantOcclusion(int x, int y, int z, Block block, int ZNLightLevel, int YNLightLevel, int YNXNLightLevel, int XNLightLevel, int XNYPLightLevel, int YPLightLevel, int YPXPLightLevel, int XPLightLevel, int XPYNLightLevel)
	{
		int texture = block.getTextureZN();
		
		GL20.glVertexAttrib1f(2, mixBrightness(ZNLightLevel, XPLightLevel, XPYNLightLevel, YNLightLevel));
		GL20.glVertexAttrib3f(1, backFace[3], backFace[4], texture);
		GL20.glVertexAttrib3f(0, backFace[0] + x, backFace[1] + y, backFace[2] + z);
		
		GL20.glVertexAttrib1f(2, mixBrightness(ZNLightLevel, YNLightLevel, YNXNLightLevel, XNLightLevel));
		GL20.glVertexAttrib3f(1, backFace[8], backFace[9], texture);
		GL20.glVertexAttrib3f(0, backFace[5] + x, backFace[6] + y, backFace[7] + z);
		
		GL20.glVertexAttrib1f(2, mixBrightness(ZNLightLevel, XNLightLevel, XNYPLightLevel, YPLightLevel));
		GL20.glVertexAttrib3f(1, backFace[13], backFace[14], texture);
		GL20.glVertexAttrib3f(0, backFace[10] + x, backFace[11] + y, backFace[12] + z);
		
		GL20.glVertexAttrib1f(2, mixBrightness(ZNLightLevel, YPLightLevel, YPXPLightLevel, XPLightLevel));
		GL20.glVertexAttrib3f(1, backFace[18], backFace[19], texture);
		GL20.glVertexAttrib3f(0, backFace[15] + x, backFace[16] + y, backFace[17] + z);
	}
	
	public static void drawXNFaceWithAmbiantOcclusion(int x, int y, int z, Block block, int XNLightLevel, int YNLightLevel, int YNZPLightLevel, int ZPLightLevel, int ZPYPLightLevel, int YPLightLevel, int YPZNLightLevel, int ZNLightLevel, int ZNYNLightLevel)
	{
		int texture = block.getTextureXN();
		
		GL20.glVertexAttrib1f(2, mixBrightness(XNLightLevel, ZNLightLevel, ZNYNLightLevel, YNLightLevel));
		GL20.glVertexAttrib3f(1, leftFace[3], leftFace[4], texture);
		GL20.glVertexAttrib3f(0, leftFace[0] + x, leftFace[1] + y, leftFace[2] + z);
		
		GL20.glVertexAttrib1f(2, mixBrightness(XNLightLevel, YNLightLevel, YNZPLightLevel, ZPLightLevel));
		GL20.glVertexAttrib3f(1, leftFace[8], leftFace[9], texture);
		GL20.glVertexAttrib3f(0, leftFace[5] + x, leftFace[6] + y, leftFace[7] + z);
		
		GL20.glVertexAttrib1f(2, mixBrightness(XNLightLevel, ZPLightLevel, ZPYPLightLevel, YPLightLevel));
		GL20.glVertexAttrib3f(1, leftFace[13], leftFace[14], texture);
		GL20.glVertexAttrib3f(0, leftFace[10] + x, leftFace[11] + y, leftFace[12] + z);
		
		GL20.glVertexAttrib1f(2, mixBrightness(XNLightLevel, YPLightLevel, YPZNLightLevel, ZNLightLevel));
		GL20.glVertexAttrib3f(1, leftFace[18], leftFace[19], texture);
		GL20.glVertexAttrib3f(0, leftFace[15] + x, leftFace[16] + y, leftFace[17] + z);
	}
	
	public static void drawYNFaceWithAmbiantOcclusion(int x, int y, int z, Block block, int YNLightLevel, int ZNLightLevel, int ZNXPLightLevel, int XPLightLevel, int XPZPLightLevel, int ZPLightLevel, int ZPXNLightLevel, int XNLightLevel, int XNZNLightLevel)
	{
		int texture = block.getTextureYN();
		
		GL20.glVertexAttrib1f(2, mixBrightness(YNLightLevel, XNLightLevel, XNZNLightLevel, ZNLightLevel));
		GL20.glVertexAttrib3f(1, bottomFace[3], bottomFace[4], texture);
		GL20.glVertexAttrib3f(0, bottomFace[0] + x, bottomFace[1] + y, bottomFace[2] + z);
		
		GL20.glVertexAttrib1f(2, mixBrightness(YNLightLevel, ZNLightLevel, ZNXPLightLevel, XPLightLevel));
		GL20.glVertexAttrib3f(1, bottomFace[8], bottomFace[9], texture);
		GL20.glVertexAttrib3f(0, bottomFace[5] + x, bottomFace[6] + y, bottomFace[7] + z);
		
		GL20.glVertexAttrib1f(2, mixBrightness(YNLightLevel, XPLightLevel, XPZPLightLevel, ZPLightLevel));
		GL20.glVertexAttrib3f(1, bottomFace[13], bottomFace[14], texture);
		GL20.glVertexAttrib3f(0, bottomFace[10] + x, bottomFace[11] + y, bottomFace[12] + z);
		
		GL20.glVertexAttrib1f(2, mixBrightness(YNLightLevel, ZPLightLevel, ZPXNLightLevel, XNLightLevel));
		GL20.glVertexAttrib3f(1, bottomFace[18], bottomFace[19], texture);
		GL20.glVertexAttrib3f(0, bottomFace[15] + x, bottomFace[16] + y, bottomFace[17] + z);
	}
	
	private static float mixBrightness(int main, int edge1, int corner, int edge2)
	{
		int smoothLighting = main;
		float divisor = 1;
		
		if (edge1 != 0)
		{
			smoothLighting += edge1;
			divisor++;
		}
		
		if (edge2 != 0)
		{
			smoothLighting += edge2;
			divisor++;
		}
		
		if (edge1 == 0 && edge2 == 0)
		{
			divisor += 0.5f;
		}
		else if (corner != 0)
		{
			smoothLighting += corner;
			divisor++;
		}
		
		smoothLighting /= divisor;
		
		return (smoothLighting - (edge1 == 0 || corner == 0 || edge2 == 0 ? 0.2f * smoothLighting : 0)) / 15.0f;
	}
}
