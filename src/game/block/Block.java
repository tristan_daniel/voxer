package game.block;

import game.item.ToolType;
import game.openGL.TextureArray;
import game.shaders.Shaders;
import game.world.BlockManager;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import start.Voxer;
import util.string.FileName;

import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

public class Block
{
	private static Block[] blocks = new Block[BlockManager.ID];
	
	static
	{
		blocks[0] = new Block("air");
		blocks[0].displayName = "Air";
		blocks[0].transparent = true;
		
		blocks[1] = new Block("void");
		blocks[1].displayName = "Void";
	}
	
	private int id = 0;
	
	private String displayName = "Unknown";
	
	private int textureYP = 0;
	private int textureZN = 0;
	private int textureXP = 0;
	private int textureZP = 0;
	private int textureXN = 0;
	private int textureYN = 0;
	
	private boolean transparent = false;
	private boolean renderSharedFaces = false;
	
	private int luminosity = 0;
	
	private float hardness = 1;
	private int efficientToolType = ToolType.none;
	
	public Block(String name)
	{
		id = getBlockIdFromName(name);
		blocks[id] = this;
	}
	
	public String getDisplayName()
	{
		return displayName;
	}
	
	public int getTextureYP()
	{
		return textureYP;
	}
	
	public int getTextureZP()
	{
		return textureZP;
	}
	
	public int getTextureXP()
	{
		return textureXP;
	}
	
	public int getTextureZN()
	{
		return textureZN;
	}
	
	public int getTextureXN()
	{
		return textureXN;
	}
	
	public int getTextureYN()
	{
		return textureYN;
	}
	
	public boolean isTransparent()
	{
		return transparent;
	}
	
	public static boolean shouldRenderFace(int mainBlock, int neightbourBlock)
	{
		if (getBlockFromId(neightbourBlock).transparent && (mainBlock != neightbourBlock || getBlockFromId(mainBlock).renderSharedFaces))
			return true;
		else
			return false;
	}
	
	public int getLuminosity()
	{
		return luminosity;
	}
	
	public float getHardness()
	{
		return hardness;
	}
	
	public int getEfficientToolType()
	{
		return efficientToolType;
	}
	
	public static Block getBlockFromId(int id)
	{
		return blocks[id];
	}
	
	public static Block getBlockFromName(String name)
	{
		return blocks[getBlockIdFromName(name)];
	}
	
	public static int getBlockIdFromName(String name)
	{
		switch (name)
		{
			case "air":
				return 0;
			case "void":
				return 1;
			default:
				byte[] data = name.getBytes();
				
				int c1 = 0xcc9e2d51;
				int c2 = 0x1b873593;
				
				int seed = 0x1a35cf1b;
				int h1 = seed;
				
				int offset = 0;
				int len = name.length();
				int roundedEnd = offset + (len & 0xfffffffc);
				
				for (int i = offset; i < roundedEnd; i += 4)
				{
					int k1 = (data[i] & 0xff) | ((data[i + 1] & 0xff) << 8) | ((data[i + 2] & 0xff) << 16) | (data[i + 3] << 24);
					k1 *= c1;
					k1 = (k1 << 15) | (k1 >>> 17);
					k1 *= c2;
					
					h1 ^= k1;
					h1 = (h1 << 13) | (h1 >>> 19);
					h1 = h1 * 5 + 0xe6546b64;
				}
				
				int k1 = 0;
				
				switch (len & 0x03)
				{
					case 3:
						k1 = (data[roundedEnd + 2] & 0xff) << 16;
						
					case 2:
						k1 |= (data[roundedEnd + 1] & 0xff) << 8;
						
					case 1:
						k1 |= data[roundedEnd] & 0xff;
						k1 *= c1;
						k1 = (k1 << 15) | (k1 >>> 17);
						k1 *= c2;
						h1 ^= k1;
				}
				
				h1 ^= len;
				
				h1 ^= h1 >>> 16;
				h1 *= 0x85ebca6b;
				h1 ^= h1 >>> 13;
				h1 *= 0xc2b2ae35;
				h1 ^= h1 >>> 16;
				h1 &= 0b00000000000000000111111111111111;
				
				return h1 + 2;
		}
	}
	
	public static void loadBlocks()
	{
		try
		{
			JsonObject blockObject;
			List<String> textureList = new ArrayList<String>();
			int totalTextureNumber = 0;
			int textureNumber;
			
			JsonValue field;
			
			File folder = new File("resources/blocks/");
			for (File file : folder.listFiles())
				if (file.isFile() && FileName.getFileExtension(file.getName()).equalsIgnoreCase("block"))
				{
					blockObject = JsonObject.readFrom(StandardCharsets.UTF_8.decode(ByteBuffer.wrap(Files.readAllBytes(Paths.get(file.toURI())))).toString());
					
					Block block = new Block(FileName.removeFileExtension(file.getName()));
					
					block.displayName = blockObject.get("name").asString();
					
					File texture;
					textureNumber = 0;
					for (int i = 0; i < 6; i++)
					{
						texture = new File("resources/textures/" + FileName.removeFileExtension(file.getName()) + "_" + i + ".png");
						if (texture.exists())
						{
							textureList.add(texture.getPath());
							textureNumber++;
						}
						else
							break;
					}
					
					field = blockObject.get("displayName");
					if (field != null)
						block.displayName = field.asString();
					
					field = blockObject.get("texture-top");
					int i = 0;
					if (field != null)
						if ((i = field.asInt()) >= 0 && i <= textureNumber)
							block.textureYP = totalTextureNumber + i;
					
					field = blockObject.get("texture-front");
					i = 0;
					if (field != null)
						if ((i = field.asInt()) >= 0 && i <= textureNumber)
							block.textureZN = totalTextureNumber + i;
					
					field = blockObject.get("texture-right");
					i = 0;
					if (field != null)
						if ((i = field.asInt()) >= 0 && i <= textureNumber)
							block.textureXP = totalTextureNumber + i;
					
					field = blockObject.get("texture-back");
					i = 0;
					if (field != null)
						if ((i = field.asInt()) >= 0 && i <= textureNumber)
							block.textureZP = totalTextureNumber + i;
					
					field = blockObject.get("texture-left");
					i = 0;
					if (field != null)
						if ((i = field.asInt()) >= 0 && i <= textureNumber)
							block.textureXN = totalTextureNumber + i;
					
					field = blockObject.get("texture-bottom");
					i = 0;
					if (field != null)
						if ((i = field.asInt()) >= 0 && i <= textureNumber)
							block.textureYN = totalTextureNumber + i;
					
					field = blockObject.get("transparent");
					if (field != null)
						if (field.asBoolean())
							block.transparent = true;
					
					field = blockObject.get("renderSharedFaces");
					if (field != null)
						if (field.asBoolean())
							block.renderSharedFaces = true;
					
					field = blockObject.get("luminosity");
					i = 0;
					if (field != null)
						if ((i = field.asInt()) > 0 && i <= 15)
							block.luminosity = i;
					
					field = blockObject.get("hardness");
					float f = 0;
					if (field != null)
						if ((f = field.asFloat()) > 0)
							block.hardness = f;
					
					field = blockObject.get("efficient-tool-type");
					if (field != null)
						switch (field.asString().toLowerCase())
						{
							case "pickaxe":
								block.efficientToolType = ToolType.pickaxe;
								break;
							case "shovel":
								block.efficientToolType = ToolType.shovel;
								break;
							case "axe":
								block.efficientToolType = ToolType.axe;
								break;
							case "shears":
								block.efficientToolType = ToolType.shears;
								break;
							default:
								block.efficientToolType = ToolType.none;
								break;
						}
					
					totalTextureNumber += textureNumber;
				}
			
			Shaders.diffuseTextures = new TextureArray(totalTextureNumber, 32);
			for (String textureName : textureList)
				Shaders.diffuseTextures.load(textureName);
			Shaders.diffuseTextures.createTexture();
			Shaders.diffuseTextures.bind();
		}
		catch (IOException e)
		{
			Voxer.error("Loading blocks : " + e.getMessage());
		}
	}
}
