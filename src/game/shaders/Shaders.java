package game.shaders;

import game.openGL.Framebuffer;
import game.openGL.Shader;
import game.openGL.ShaderProgram;
import game.openGL.Texture;
import game.openGL.TextureArray;
import game.openGL.Uniform;
import game.openGL.UniformMatrix4f;
import game.openGL.UniformTexture;
import game.player.Player;
import game.player.PlayerCamera;
import game.world.BlockManager;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

public class Shaders
{
	private static final boolean USE_SHADOWMAP = false;
	
	private static ShaderProgram mainProgram;
	public static TextureArray diffuseTextures;
	private static UniformMatrix4f viewProjectionMatrixUniform;
	private static UniformMatrix4f depthViewProjectionMatrixUniform;
	private static UniformTexture diffuseTexturesUniform;
	private static UniformTexture shadowMapTextureUniform;
	
	private static ShaderProgram shadowMapProgram;
	private static UniformMatrix4f shadowMapViewProjectionMatrixUniform;
	private static Texture shadowMapTexture;
	private static Framebuffer shadowMapFramebuffer;
	private static int shadowMapWidth = 4096;
	private static int shadowMapHeight = 4096;
	
	private static ShaderProgram skyProgram;
	private static UniformMatrix4f skyViewProjectionMatrixUniform;
	private static UniformMatrix4f skyViewMatrixInverseUniform;
	private static Uniform skyCameraPositionUniform;
	private static Uniform skySunPositionUniform;
	private static Uniform skyTurbidityUniform;
	private static Uniform skyReileighUniform;
	
	public static void init()
	{
		if (USE_SHADOWMAP)
			initShadowMap();
		initNormal();
	}
	
	public static void render()
	{
		if (USE_SHADOWMAP)
			shadowMapPass();
		
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		
		normalPass();
		drawCursor();
	}
	
	private static void initShadowMap()
	{
		shadowMapProgram = new ShaderProgram();
		shadowMapProgram.attachShader(new Shader("resources/shaders/shadow.vs", Shader.VERTEX_SHADER));
		shadowMapProgram.attachShader(new Shader("resources/shaders/shadow.fs", Shader.FRAGMENT_SHADER));
		shadowMapProgram.bindAttribLocation(0, "in_Position");
		shadowMapProgram.linkAndValidate();
		shadowMapProgram.bind();
		shadowMapProgram.unbind();
		
		shadowMapViewProjectionMatrixUniform = new UniformMatrix4f(shadowMapProgram, "viewProjectionMatrix");
		
		shadowMapTexture = new Texture();
		shadowMapTexture.bind();
		shadowMapTexture.allocateMemory(shadowMapWidth, shadowMapHeight, GL11.GL_DEPTH_COMPONENT);
		shadowMapTexture.setParameter(GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		shadowMapTexture.setParameter(GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		shadowMapTexture.setParameter(GL11.GL_TEXTURE_WRAP_S, GL13.GL_CLAMP_TO_BORDER);
		shadowMapTexture.setParameter(GL11.GL_TEXTURE_WRAP_T, GL13.GL_CLAMP_TO_BORDER);
		shadowMapTexture.setParameter(GL14.GL_TEXTURE_COMPARE_MODE, GL30.GL_COMPARE_REF_TO_TEXTURE);
		shadowMapTexture.setParameter(GL14.GL_TEXTURE_COMPARE_FUNC, GL11.GL_LEQUAL);
		shadowMapTexture.unbind();
		
		shadowMapFramebuffer = new Framebuffer();
		shadowMapFramebuffer.bind();
		shadowMapFramebuffer.setDepthAttachment(shadowMapTexture);
		shadowMapFramebuffer.unbind();
	}
	
	private static void initNormal()
	{
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glDepthFunc(GL11.GL_LESS);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glCullFace(GL11.GL_BACK);
		GL11.glFrontFace(GL11.GL_CCW);
		
		if (!USE_SHADOWMAP)
		{
			GL11.glEnable(GL11.GL_ALPHA_TEST);
			GL11.glAlphaFunc(GL11.GL_GREATER, 0.5f);
		}
		
		mainProgram = new ShaderProgram();
		
		if (USE_SHADOWMAP)
		{
			mainProgram.attachShader(new Shader("resources/shaders/main.vs", Shader.VERTEX_SHADER));
			mainProgram.attachShader(new Shader("resources/shaders/main.fs", Shader.FRAGMENT_SHADER));
		}
		else
		{
			mainProgram.attachShader(new Shader("resources/shaders/mainNoShadow.vs", Shader.VERTEX_SHADER));
			mainProgram.attachShader(new Shader("resources/shaders/mainNoShadow.fs", Shader.FRAGMENT_SHADER));
		}
		mainProgram.bindAttribLocation(0, "in_Position");
		mainProgram.bindAttribLocation(1, "in_TextureCoord");
		mainProgram.bindAttribLocation(2, "in_Lighting");
		mainProgram.linkAndValidate();
		
		diffuseTexturesUniform = new UniformTexture(mainProgram, "diffuseTextures", diffuseTextures);
		viewProjectionMatrixUniform = new UniformMatrix4f(mainProgram, "viewProjectionMatrix");
		if (USE_SHADOWMAP)
		{
			shadowMapTextureUniform = new UniformTexture(mainProgram, "shadowMap", shadowMapTexture);
			depthViewProjectionMatrixUniform = new UniformMatrix4f(mainProgram, "depthViewProjectionMatrix");
		}
	}
	
	private static void initSky()
	{
		skyProgram = new ShaderProgram();
		skyProgram.attachShader(new Shader("resources/shaders/sky.vs", Shader.VERTEX_SHADER));
		skyProgram.attachShader(new Shader("resources/shaders/sky.fs", Shader.FRAGMENT_SHADER));
		skyProgram.bindAttribLocation(0, "in_Position");
		skyProgram.linkAndValidate();
		
		skyViewProjectionMatrixUniform = new UniformMatrix4f(skyProgram, "viewProjectionMatrix");
	}
	
	private static void shadowMapPass()
	{
		shadowMapFramebuffer.startDrawingOn(shadowMapWidth, shadowMapHeight);
		
		GL11.glCullFace(GL11.GL_FRONT);
		GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);
		
		shadowMapProgram.bind();
		shadowMapViewProjectionMatrixUniform.upload(Sky.viewProjectionMatrix);
		
		BlockManager.render();
		
		shadowMapProgram.unbind();
		
		GL11.glCullFace(GL11.GL_BACK);
		
		shadowMapFramebuffer.stopDrawingOn();
	}
	
	private static void normalPass()
	{
		mainProgram.bind();
		viewProjectionMatrixUniform.upload(PlayerCamera.viewProjectionMatrix);
		
		diffuseTexturesUniform.bindOnUnit(0);
		if (USE_SHADOWMAP)
			shadowMapTextureUniform.bindOnUnit(1);
		
		BlockManager.render();
		BlockManager.renderSelectedBlockBoundingBox();
		
		mainProgram.unbind();
	}
	
	private static void skyPass()
	{
		skyProgram.bind();
		
		skyViewProjectionMatrixUniform.upload(PlayerCamera.viewProjectionMatrix);
		skyViewMatrixInverseUniform.upload(PlayerCamera.viewMatrixInverse);
		skyCameraPositionUniform.upload(Player.position);
		skySunPositionUniform.upload(Sky.sunPosition);
		skyTurbidityUniform.upload(1000);
		skyReileighUniform.upload(1000);
		
		Sky.render();
		
		skyProgram.unbind();
	}
	
	private static void drawCursor()
	{
		GL11.glBegin(GL11.GL_QUADS);
		GL20.glVertexAttrib3f(0, 0.0025f, 0.005f, 0);
		GL20.glVertexAttrib3f(0, -0.0025f, 0.005f, 0);
		GL20.glVertexAttrib3f(0, -0.0025f, -0.005f, 0);
		GL20.glVertexAttrib3f(0, 0.0025f, -0.005f, 0);
		GL11.glEnd();
	}
	
	public static void destroy()
	{
		mainProgram.destroy();
		diffuseTextures.destroy();
		
		if (USE_SHADOWMAP)
		{
			shadowMapProgram.destroy();
			shadowMapTexture.destroy();
			shadowMapFramebuffer.destroy();
		}
	}
}
