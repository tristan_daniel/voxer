package game.shaders;

import game.player.Player;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import util.math.Mat4f;
import util.math.Vec3f;

public class Sky
{
	private static final int zero = -512;
	private static final int one = 512;
	
	private static final float[] bottomSkyboxFace = { zero, one, one, one, one, one, one, one, zero, zero, one, zero };
	private static final float[] backSkyboxFace = { zero, zero, one, one, zero, one, one, one, one, zero, one, one };
	private static final float[] leftSkyboxFace = { one, zero, one, one, zero, zero, one, one, zero, one, one, one };
	private static final float[] frontSkyboxFace = { one, zero, zero, zero, zero, zero, zero, one, zero, one, one, zero };
	private static final float[] rightSkyboxFace = { zero, zero, zero, zero, zero, one, zero, one, one, zero, one, zero };
	private static final float[] topSkyboxFace = { zero, zero, zero, one, zero, zero, one, zero, one, zero, zero, one };
	
	public static Vec3f sunPosition = new Vec3f(8f, 150f, 8f);
	
	private static Mat4f projectionMatrix = new Mat4f();
	private static Mat4f viewMatrix = new Mat4f();
	private static Mat4f bias = new Mat4f();
	static
	{
		bias.m00 = bias.m11 = bias.m22 = bias.m03 = bias.m13 = bias.m23 = 0.5f;
		bias.m33 = 1.0f;
	}
	
	public static Mat4f viewProjectionMatrix = new Mat4f();
	
	public static void init()
	{
		projectionMatrix.setOrthographicProjection(128, -128, 128, -128, 1, 512);
		// projectionMatrix.set(PlayerCamera.projectionMatrix);
	}
	
	public static void update()
	{
		viewMatrix.lookAt(Player.position.x, Player.position.y, Player.position.z, 0, 100, 0);
		// viewProjectionMatrix.set(PlayerCamera.viewProjectionMatrix);
		Mat4f.multiply(projectionMatrix, viewMatrix, viewProjectionMatrix);
	}
	
	public static void render()
	{
		GL11.glBegin(GL11.GL_QUADS);
		GL20.glVertexAttrib3f(0, topSkyboxFace[9] + Player.position.x, topSkyboxFace[10], topSkyboxFace[11] + Player.position.z);
		GL20.glVertexAttrib3f(0, topSkyboxFace[6] + Player.position.x, topSkyboxFace[7], topSkyboxFace[8] + Player.position.z);
		GL20.glVertexAttrib3f(0, topSkyboxFace[3] + Player.position.x, topSkyboxFace[4], topSkyboxFace[5] + Player.position.z);
		GL20.glVertexAttrib3f(0, topSkyboxFace[0] + Player.position.x, topSkyboxFace[1], topSkyboxFace[2] + Player.position.z);
		
		GL20.glVertexAttrib3f(0, rightSkyboxFace[9] + Player.position.x, rightSkyboxFace[10], rightSkyboxFace[11] + Player.position.z);
		GL20.glVertexAttrib3f(0, rightSkyboxFace[6] + Player.position.x, rightSkyboxFace[7], rightSkyboxFace[8] + Player.position.z);
		GL20.glVertexAttrib3f(0, rightSkyboxFace[3] + Player.position.x, rightSkyboxFace[4], rightSkyboxFace[5] + Player.position.z);
		GL20.glVertexAttrib3f(0, rightSkyboxFace[0] + Player.position.x, rightSkyboxFace[1], rightSkyboxFace[2] + Player.position.z);
		
		GL20.glVertexAttrib3f(0, backSkyboxFace[9] + Player.position.x, backSkyboxFace[10], backSkyboxFace[11] + Player.position.z);
		GL20.glVertexAttrib3f(0, backSkyboxFace[6] + Player.position.x, backSkyboxFace[7], backSkyboxFace[8] + Player.position.z);
		GL20.glVertexAttrib3f(0, backSkyboxFace[3] + Player.position.x, backSkyboxFace[4], backSkyboxFace[5] + Player.position.z);
		GL20.glVertexAttrib3f(0, backSkyboxFace[0] + Player.position.x, backSkyboxFace[1], backSkyboxFace[2] + Player.position.z);
		
		GL20.glVertexAttrib3f(0, leftSkyboxFace[9] + Player.position.x, leftSkyboxFace[10], leftSkyboxFace[11] + Player.position.z);
		GL20.glVertexAttrib3f(0, leftSkyboxFace[6] + Player.position.x, leftSkyboxFace[7], leftSkyboxFace[8] + Player.position.z);
		GL20.glVertexAttrib3f(0, leftSkyboxFace[3] + Player.position.x, leftSkyboxFace[4], leftSkyboxFace[5] + Player.position.z);
		GL20.glVertexAttrib3f(0, leftSkyboxFace[0] + Player.position.x, leftSkyboxFace[1], leftSkyboxFace[2] + Player.position.z);
		
		GL20.glVertexAttrib3f(0, frontSkyboxFace[9] + Player.position.x, frontSkyboxFace[10], frontSkyboxFace[11] + Player.position.z);
		GL20.glVertexAttrib3f(0, frontSkyboxFace[6] + Player.position.x, frontSkyboxFace[7], frontSkyboxFace[8] + Player.position.z);
		GL20.glVertexAttrib3f(0, frontSkyboxFace[3] + Player.position.x, frontSkyboxFace[4], frontSkyboxFace[5] + Player.position.z);
		GL20.glVertexAttrib3f(0, frontSkyboxFace[0] + Player.position.x, frontSkyboxFace[1], frontSkyboxFace[2] + Player.position.z);
		
		GL20.glVertexAttrib3f(0, bottomSkyboxFace[9] + Player.position.x, bottomSkyboxFace[10], bottomSkyboxFace[11] + Player.position.z);
		GL20.glVertexAttrib3f(0, bottomSkyboxFace[6] + Player.position.x, bottomSkyboxFace[7], bottomSkyboxFace[8] + Player.position.z);
		GL20.glVertexAttrib3f(0, bottomSkyboxFace[3] + Player.position.x, bottomSkyboxFace[4], bottomSkyboxFace[5] + Player.position.z);
		GL20.glVertexAttrib3f(0, bottomSkyboxFace[0] + Player.position.x, bottomSkyboxFace[1], bottomSkyboxFace[2] + Player.position.z);
		GL11.glEnd();
	}
}
