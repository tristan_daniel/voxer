package game.item;

public class ToolType
{
	public static final int none = 0;
	public static final int pickaxe = 1;
	public static final int shovel = 2;
	public static final int axe = 3;
	public static final int shears = 4;
}
