package game.world;

import game.block.Block;
import game.block.BlockTesselator;
import game.player.Player;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import start.Voxer;
import util.math.MathHelper;
import util.math.Vec2i;
import util.math.Vec3i;
import util.time.Benchmark;

public class BlockManager
{
	public static Chunk[] chunks;
	public static Chunk chunkNull = new ChunkNull();
	
	private static int[] blocks;
	private static int[] skyLightMap;
	
	private static int viewDistanceInChunks = 0;
	private static int positiveViewDistanceInChunks = 0;
	private static int negativeViewDistanceInChunks = 0;
	private static int negativeViewDistanceInBlocks = 0;
	
	private static int mapWidthInChunks = 0;
	private static int mapWidthInBlocks = 0;
	
	public static Vec2i centerChunk = new Vec2i(0, 0);
	private static Vec2i centerBlock = new Vec2i(0, 0);
	
	private static Vec2i chunkShiftInChunks = new Vec2i(0, 0);
	private static Vec2i chunkShiftInBlocks = new Vec2i(chunkShiftInChunks.x * Chunk.WIDTH, chunkShiftInChunks.z * Chunk.WIDTH);
	
	public static Vec3i selectedBlockForPlacing = new Vec3i(0, 0, 0);
	public static Vec3i selectedBlock = new Vec3i(0, 0, 0);
	public static boolean blockSelected;
	
	// 15 : id -> 32768
	public static final int ID = 0b00000000000000000111111111111111;
	public static final int ID_INVERSE = ~ID & -1;
	// 4 : artificial light -> 16
	public static final int ARTIFICIAL_LIGHT_LEVEL = 0b0000000000001111000000000000000;
	public static final int ARTIFICIAL_LIGHT_LEVEL_INVERSE = ~ARTIFICIAL_LIGHT_LEVEL & -1;
	public static final int ARTIFICIAL_LIGHT_LEVEL_OFFSET = 15;
	// 1 : artificial light flag
	public static final int ARTIFICIAL_LIGHT_FLAG = 0b00000000000010000000000000000000;
	public static final int ARTIFICIAL_LIGHT_FLAG_INVERSE = ~ARTIFICIAL_LIGHT_LEVEL & -1;
	public static final int ARTIFICIAL_LIGHT_FLAG_OFFSET = 19;
	// 4 : sun light -> 16
	public static final int SUN_LIGHT_LEVEL = 0b00000000111100000000000000000000;
	public static final int SUN_LIGHT_LEVEL_INVERSE = ~SUN_LIGHT_LEVEL & -1;
	public static final int SUN_LIGHT_LEVEL_OFFSET = 20;
	// 1 : sun light flag
	public static final int SUN_LIGHT_FLAG = 0b00000001000000000000000000000000;
	public static final int SUN_LIGHT_FLAG_INVERSE = ~SUN_LIGHT_LEVEL & -1;
	public static final int SUN_LIGHT_FLAG_OFFSET = 24;
	// 5 : orientation -> 32
	public static final int ORIENTATION = 0b00111110000000000000000000000000;
	public static final int ORIENTATION_INVERSE = ~ORIENTATION & -1;
	public static final int ORIENTATION_OFFSET = 25;
	
	public static final int ORIENTATION_TOP_0_DEGREES = 0;
	public static final int ORIENTATION_TOP_90_DEGREES = 1;
	public static final int ORIENTATION_TOP_180_DEGREES = 2;
	public static final int ORIENTATION_TOP_270_DEGREES = 3;
	public static final int ORIENTATION_NORTH_0_DEGREES = 4;
	public static final int ORIENTATION_NORTH_90_DEGREES = 5;
	public static final int ORIENTATION_NORTH_180_DEGREES = 6;
	public static final int ORIENTATION_NORTH_270_DEGREES = 7;
	public static final int ORIENTATION_EAST_0_DEGREES = 8;
	public static final int ORIENTATION_EAST_90_DEGREES = 9;
	public static final int ORIENTATION_EAST_180_DEGREES = 10;
	public static final int ORIENTATION_EAST_270_DEGREES = 11;
	public static final int ORIENTATION_SOUTH_0_DEGREES = 12;
	public static final int ORIENTATION_SOUTH_90_DEGREES = 13;
	public static final int ORIENTATION_SOUTH_180_DEGREES = 14;
	public static final int ORIENTATION_SOUTH_270_DEGREES = 15;
	public static final int ORIENTATION_WEST_0_DEGREES = 16;
	public static final int ORIENTATION_WEST_90_DEGREES = 17;
	public static final int ORIENTATION_WEST_180_DEGREES = 18;
	public static final int ORIENTATION_WEST_270_DEGREES = 19;
	public static final int ORIENTATION_BOTTOM_0_DEGREES = 20;
	public static final int ORIENTATION_BOTTOM_90_DEGREES = 21;
	public static final int ORIENTATION_BOTTOM_180_DEGREES = 22;
	public static final int ORIENTATION_BOTTOM_270_DEGREES = 23;
	
	// 2 : protection -> 4
	public static final int PROTECTION_LEVEL = 0b11000000000000000000000000000000;
	public static final int PROTECTION_LEVEL_INVERSE = ~PROTECTION_LEVEL & -1;
	public static final int PROTECTION_LEVEL_OFFSET = 30;
	
	public static final boolean SMOOTH_LIGHTING = true;
	
	public static void init(int viewDistanceInBlocks)
	{
		viewDistanceInChunks = viewDistanceInBlocks / Chunk.WIDTH;
		mapWidthInChunks = viewDistanceInChunks * 2 + 1;
		mapWidthInBlocks = mapWidthInChunks * Chunk.WIDTH;
		positiveViewDistanceInChunks = viewDistanceInChunks + 1;
		negativeViewDistanceInChunks = viewDistanceInChunks;
		negativeViewDistanceInBlocks = negativeViewDistanceInChunks * Chunk.WIDTH;
		
		chunks = new Chunk[mapWidthInChunks * mapWidthInChunks];
		blocks = new int[mapWidthInBlocks * mapWidthInBlocks * Chunk.HEIGHT];
		skyLightMap = new int[mapWidthInBlocks * mapWidthInBlocks];
		
		for (int x = -negativeViewDistanceInChunks; x < positiveViewDistanceInChunks; x++)
			for (int z = -negativeViewDistanceInChunks; z < positiveViewDistanceInChunks; z++)
			{
				int coords = inlineChunkCoords(x, z);
				chunks[coords] = new Chunk();
				chunks[coords].load(x, z);
				WorldGenerator.generateChunk(x, z);
			}
		updateSunLighting();
		rebuildAll();
	}
	
	public static void rebuildAll()
	{
		for (Chunk chunk : chunks)
			chunk.needsGeometryRebuild = true;
	}
	
	public static void render()
	{
		for (Chunk chunk : BlockManager.chunks)
			GL11.glCallList(chunk.displayListId);
	}
	
	public static void setCenterChunk(int x, int z)
	{
		
	}
	
	public static void shiftChunks(int x, int z)
	{
		Voxer.debug(x > 1);
		
		x = MathHelper.signum(x);
		z = MathHelper.signum(z);
		
		centerChunk.x += x;
		centerChunk.z += z;
		
		centerBlock.x = centerChunk.x * Chunk.WIDTH;
		centerBlock.z = centerChunk.z * Chunk.WIDTH;
		
		chunkShiftInChunks.x += x;
		chunkShiftInChunks.z += z;
		
		chunkShiftInBlocks.x = chunkShiftInChunks.x * Chunk.WIDTH;
		chunkShiftInBlocks.z = chunkShiftInChunks.z * Chunk.WIDTH;
		
		if (x == 1)
			for (int Z = centerChunk.z - viewDistanceInChunks; Z < centerChunk.z + viewDistanceInChunks; Z++)
				chunks[inlineChunkCoords(centerChunk.x + viewDistanceInChunks, Z)].load(centerChunk.x + viewDistanceInChunks, Z);
		else if (x == -1)
			for (int Z = centerChunk.z - viewDistanceInChunks; Z < centerChunk.z + viewDistanceInChunks; Z++)
				chunks[inlineChunkCoords(centerChunk.x - viewDistanceInChunks, Z)].load(centerChunk.x - viewDistanceInChunks, Z);
		else if (z == 1)
			for (int X = centerChunk.x - viewDistanceInChunks; X < centerChunk.x + viewDistanceInChunks; X++)
				chunks[inlineChunkCoords(X, centerChunk.z + viewDistanceInChunks)].load(X, centerChunk.z + viewDistanceInChunks);
		else if (z == -1)
			for (int X = centerChunk.x - viewDistanceInChunks; X < centerChunk.x + viewDistanceInChunks; X++)
			{
				Voxer.debug(inlineChunkCoords(X, centerChunk.z - viewDistanceInChunks));
				chunks[inlineChunkCoords(X, centerChunk.z - viewDistanceInChunks)].load(X, centerChunk.z - viewDistanceInChunks);
			}
	}
	
	public static void update()
	{
		Benchmark.start();
		updateSunLighting();
		updateArtificialLighting();
		updateGeometry();
		updatePointedBlockCoordinates();
		Benchmark.displayTimeIfGreaterThan(3000000, "Chunk update : ");
	}
	
	private static void updateSunLighting()
	{
		for (Chunk chunk : chunks)
			if (chunk.isModified)
			{
				chunk.isBeingLit = true;
				
				setChunkSunLightLevel(chunk, 1);
				
				int xMax = chunk.offset.x + Chunk.WIDTH;
				int zMax = chunk.offset.z + Chunk.WIDTH;
				
				for (int x = chunk.offset.x; x < xMax; x++)
					for (int z = chunk.offset.z; z < zMax; z++)
					{
						int y;
						for (y = Chunk.HEIGHT; y > 0 && isBlockTransparent(x, y, z); y--)
							setBlockSunLightLevel(x, y, z, 15);
						skyLightMap[inline2DBlockCoords(x, z)] = y + 1;
						propagateSunLight(x, y + 1, z, 15, chunk.position.x, chunk.position.z);
					}
			}
		
		for (int i = 0; i < 3; i++)
			for (Chunk chunk : chunks)
				if (chunk.isBeingLit)
				{
					int xMin = chunk.offset.x;
					int zMin = chunk.offset.z;
					int xMax = chunk.offset.x + Chunk.WIDTH;
					int zMax = chunk.offset.z + Chunk.WIDTH;
					
					for (int y = 0; y < Chunk.HEIGHT; y++)
					{
						for (int x = xMin; x < xMax; x++)
						{
							int insideLightLevel = getBlockSunLightLevel(x, y, zMax - 1);
							if (insideLightLevel != 0)
								if (insideLightLevel != 0)
								{
									int outsideLightLevel = getBlockSunLightLevel(x, y, zMax) - 1;
									if (outsideLightLevel > insideLightLevel)
										propagateSunLight(x, y, zMax - 1, outsideLightLevel, chunk.position.x, chunk.position.z);
								}
							
							insideLightLevel = getBlockSunLightLevel(x, y, zMin);
							if (insideLightLevel != 0)
							{
								int outsideLightLevel = getBlockSunLightLevel(x, y, zMin - 1) - 1;
								if (outsideLightLevel > insideLightLevel)
									propagateSunLight(x, y, zMin, outsideLightLevel, chunk.position.x, chunk.position.z);
							}
						}
						
						for (int z = zMin; z < zMax; z++)
						{
							int insideLightLevel = getBlockSunLightLevel(xMax - 1, y, z);
							if (insideLightLevel != 0)
							{
								int outsideLightLevel = getBlockSunLightLevel(xMax, y, z) - 1;
								if (outsideLightLevel > insideLightLevel)
									propagateSunLight(xMax - 1, y, z, outsideLightLevel, chunk.position.x, chunk.position.z);
							}
							
							insideLightLevel = getBlockSunLightLevel(xMin, y, z);
							if (insideLightLevel != 0)
							{
								int outsideLightLevel = getBlockSunLightLevel(xMin - 1, y, z) - 1;
								if (outsideLightLevel > insideLightLevel)
									propagateSunLight(xMin, y, z, outsideLightLevel, chunk.position.x, chunk.position.z);
							}
						}
					}
				}
		
		for (Chunk chunk : chunks)
			if (chunk.isBeingLit)
				chunk.isBeingLit = false;
	}
	
	public static void updateArtificialLighting()
	{
		for (Chunk chunk : chunks)
			if (chunk.needsLightingUpdate)
			{
				int xMax = chunk.offset.x + Chunk.WIDTH;
				int zMax = chunk.offset.z + Chunk.WIDTH;
				
				for (int x = chunk.offset.x; x < xMax; x++)
					for (int y = 0; y < Chunk.HEIGHT; y++)
						for (int z = chunk.offset.z; z < zMax; z++)
							if (getBlockArtificialLightFlag(x, y, z))
							{
								setBlockArtificialLightFlag(x, y, z, false);
								
								Block block = Block.getBlockFromId(getBlockId(x, y, z));
								int lightLevel = MathHelper.max(block.getLuminosity(), getBlockArtificialLightLevel(x, y, z));
								
								if (block.isTransparent())
								{
									lightLevel = MathHelper.max(lightLevel, 1);
									lightLevel = MathHelper.max(lightLevel, getBlockArtificialLightLevel(x, y + 1, z) - 1);
									lightLevel = MathHelper.max(lightLevel, getBlockArtificialLightLevel(x, y, z + 1) - 1);
									lightLevel = MathHelper.max(lightLevel, getBlockArtificialLightLevel(x + 1, y, z) - 1);
									lightLevel = MathHelper.max(lightLevel, getBlockArtificialLightLevel(x, y, z - 1) - 1);
									lightLevel = MathHelper.max(lightLevel, getBlockArtificialLightLevel(x - 1, y, z) - 1);
									lightLevel = MathHelper.max(lightLevel, getBlockArtificialLightLevel(x, y - 1, z) - 1);
									
									propagateArtificialLight(x, y, z, lightLevel);
								}
								else if (lightLevel > 0)
									propagateArtificialLight(x, y, z, lightLevel);
							}
				
				chunk.needsLightingUpdate = false;
			}
	}
	
	public static void updateGeometry()
	{
		for (Chunk chunk : chunks)
			if (chunk.isModified)
			{
				if (SMOOTH_LIGHTING)
					generateChunkGeometryWithAmbiantOcclusion(chunk);
				else
					generateChunkGeometry(chunk);
				chunk.isModified = false;
			}
	}
	
	public static void propagateSunLight(int x, int y, int z, int lightLevel, int chunkX, int chunkZ)
	{
		if (getChunkCoordXFromBlockCoordX(x) != chunkX || getChunkCoordZFromBlockCoordZ(z) != chunkZ)
			return;
		
		setBlockSunLightLevel(x, y, z, lightLevel);
		lightLevel--;
		
		if (lightLevel <= 1)
			return;
		
		if (isBlockTransparent(x, y + 1, z) && getBlockSunLightLevel(x, y + 1, z) < lightLevel)
			propagateSunLight(x, y + 1, z, lightLevel, chunkX, chunkZ);
		if (isBlockTransparent(x, y, z + 1) && getBlockSunLightLevel(x, y, z + 1) < lightLevel)
			propagateSunLight(x, y, z + 1, lightLevel, chunkX, chunkZ);
		if (isBlockTransparent(x + 1, y, z) && getBlockSunLightLevel(x + 1, y, z) < lightLevel)
			propagateSunLight(x + 1, y, z, lightLevel, chunkX, chunkZ);
		if (isBlockTransparent(x, y, z - 1) && getBlockSunLightLevel(x, y, z - 1) < lightLevel)
			propagateSunLight(x, y, z - 1, lightLevel, chunkX, chunkZ);
		if (isBlockTransparent(x - 1, y, z) && getBlockSunLightLevel(x - 1, y, z) < lightLevel)
			propagateSunLight(x - 1, y, z, lightLevel, chunkX, chunkZ);
		if (isBlockTransparent(x, y - 1, z) && getBlockSunLightLevel(x, y - 1, z) < lightLevel)
			propagateSunLight(x, y - 1, z, lightLevel, chunkX, chunkZ);
	}
	
	public static void propagateSunLight(int x, int y, int z, int lightLevel)
	{
		setBlockSunLightLevel(x, y, z, lightLevel);
		getChunkFromBlockCoords(x, z).isModified = true;
		lightLevel--;
		
		if (lightLevel <= 1)
			return;
		
		if (isBlockTransparent(x, y + 1, z) && getBlockSunLightLevel(x, y + 1, z) < lightLevel)
			propagateSunLight(x, y + 1, z, lightLevel);
		if (isBlockTransparent(x, y, z + 1) && getBlockSunLightLevel(x, y, z + 1) < lightLevel)
			propagateSunLight(x, y, z + 1, lightLevel);
		if (isBlockTransparent(x + 1, y, z) && getBlockSunLightLevel(x + 1, y, z) < lightLevel)
			propagateSunLight(x + 1, y, z, lightLevel);
		if (isBlockTransparent(x, y, z - 1) && getBlockSunLightLevel(x, y, z - 1) < lightLevel)
			propagateSunLight(x, y, z - 1, lightLevel);
		if (isBlockTransparent(x - 1, y, z) && getBlockSunLightLevel(x - 1, y, z) < lightLevel)
			propagateSunLight(x - 1, y, z, lightLevel);
		if (isBlockTransparent(x, y - 1, z) && getBlockSunLightLevel(x, y - 1, z) < lightLevel)
			propagateSunLight(x, y - 1, z, lightLevel);
	}
	
	public static void propagateArtificialLight(int x, int y, int z, int lightLevel)
	{
		setBlockArtificialLightLevel(x, y, z, lightLevel);
		getChunkFromBlockCoords(x, z).isModified = true;
		lightLevel--;
		
		if (lightLevel <= 1)
			return;
		
		if (isBlockTransparent(x, y + 1, z) && getBlockArtificialLightLevel(x, y + 1, z) < lightLevel)
			propagateArtificialLight(x, y + 1, z, lightLevel);
		if (isBlockTransparent(x, y, z + 1) && getBlockArtificialLightLevel(x, y, z + 1) < lightLevel)
			propagateArtificialLight(x, y, z + 1, lightLevel);
		if (isBlockTransparent(x + 1, y, z) && getBlockArtificialLightLevel(x + 1, y, z) < lightLevel)
			propagateArtificialLight(x + 1, y, z, lightLevel);
		if (isBlockTransparent(x, y, z - 1) && getBlockArtificialLightLevel(x, y, z - 1) < lightLevel)
			propagateArtificialLight(x, y, z - 1, lightLevel);
		if (isBlockTransparent(x - 1, y, z) && getBlockArtificialLightLevel(x - 1, y, z) < lightLevel)
			propagateArtificialLight(x - 1, y, z, lightLevel);
		if (isBlockTransparent(x, y - 1, z) && getBlockArtificialLightLevel(x, y - 1, z) < lightLevel)
			propagateArtificialLight(x, y - 1, z, lightLevel);
	}
	
	public static void propagateSunLightUpdate(int centerX, int centerY, int centerZ)
	{
		int y;
		
		for (y = Chunk.HEIGHT; y > 0 && isBlockAir(centerX, y, centerZ); y--)
			setBlockSunLightLevel(centerX, y, centerZ, 15);
		
		int newHeight = y + 1;
		int oldHeight = skyLightMap[inline2DBlockCoords(centerX, centerZ)];
		
		if (oldHeight > newHeight)
			for (y = oldHeight; y > newHeight; y--)
				setBlockSunLightFlag(centerX, y, centerZ, true);
		else if (newHeight > oldHeight)
			for (y = newHeight; y > oldHeight; y--)
				setBlockSunLightFlag(centerX, y, centerZ, true);
		
		skyLightMap[inline2DBlockCoords(centerX, centerZ)] = newHeight;
		
		for (int x = 0; x < 15; x++)
			for (int z = 0; z < 15 - x; z++)
				for (y = 0; y < 15 - x - z; y++)
				{
					setBlockSunLightFlag(centerX + x, newHeight + y, centerZ + z, true);
					setBlockSunLightFlag(centerX - x, newHeight + y, centerZ + z, true);
					setBlockSunLightFlag(centerX + x, newHeight + y, centerZ - z, true);
					setBlockSunLightFlag(centerX - x, newHeight + y, centerZ - z, true);
					setBlockSunLightFlag(centerX + x, newHeight - y, centerZ + z, true);
					setBlockSunLightFlag(centerX - x, newHeight - y, centerZ + z, true);
					setBlockSunLightFlag(centerX + x, newHeight - y, centerZ - z, true);
					setBlockSunLightFlag(centerX - x, newHeight - y, centerZ - z, true);
					
					setBlockSunLightFlag(centerX + x, oldHeight + y, centerZ + z, true);
					setBlockSunLightFlag(centerX - x, oldHeight + y, centerZ + z, true);
					setBlockSunLightFlag(centerX + x, oldHeight + y, centerZ - z, true);
					setBlockSunLightFlag(centerX - x, oldHeight + y, centerZ - z, true);
					setBlockSunLightFlag(centerX + x, oldHeight - y, centerZ + z, true);
					setBlockSunLightFlag(centerX - x, oldHeight - y, centerZ + z, true);
					setBlockSunLightFlag(centerX + x, oldHeight - y, centerZ - z, true);
					setBlockSunLightFlag(centerX - x, oldHeight - y, centerZ - z, true);
				}
	}
	
	private static void propagateArtificialLightUpdate(int centerX, int centerY, int centerZ)
	{
		for (int x = 0; x < 15; x++)
			for (int z = 0; z < 15 - x; z++)
				for (int y = 0; y < 15 - x - z; y++)
				{
					setBlockArtificialLightFlag(centerX + x, centerY + y, centerZ + z, true);
					setBlockArtificialLightFlag(centerX - x, centerY + y, centerZ + z, true);
					setBlockArtificialLightFlag(centerX + x, centerY + y, centerZ - z, true);
					setBlockArtificialLightFlag(centerX - x, centerY + y, centerZ - z, true);
					setBlockArtificialLightFlag(centerX + x, centerY - y, centerZ + z, true);
					setBlockArtificialLightFlag(centerX - x, centerY - y, centerZ + z, true);
					setBlockArtificialLightFlag(centerX + x, centerY - y, centerZ - z, true);
					setBlockArtificialLightFlag(centerX - x, centerY - y, centerZ - z, true);
				}
	}
	
	private static void generateChunkGeometry(Chunk chunk)
	{
		GL11.glNewList(chunk.displayListId, GL11.GL_COMPILE);
		GL11.glBegin(GL11.GL_QUADS);
		
		int xMax = chunk.offset.x + Chunk.WIDTH;
		int zMax = chunk.offset.z + Chunk.WIDTH;
		
		for (int x = chunk.offset.x; x < xMax; x++)
			for (int y = 0; y < Chunk.HEIGHT; y++)
				for (int z = chunk.offset.z; z < zMax; z++)
				{
					int id = getBlockId(x, y, z);
					if (!isBlockAir(x, y, z))
					{
						Block block = Block.getBlockFromId(id);
						
						if (isBlockTransparent(x, y + 1, z))
							BlockTesselator.drawYPFace(x, y, z, block, getBlockLightLevel(x, y + 1, z) / 15f);
						if (isBlockTransparent(x, y, z + 1))
							BlockTesselator.drawZPFace(x, y, z, block, getBlockLightLevel(x, y, z + 1) / 15f);
						if (isBlockTransparent(x + 1, y, z))
							BlockTesselator.drawXPFace(x, y, z, block, getBlockLightLevel(x + 1, y, z) / 15f);
						if (isBlockTransparent(x, y, z - 1))
							BlockTesselator.drawZNFace(x, y, z, block, getBlockLightLevel(x, y, z - 1) / 15f);
						if (isBlockTransparent(x - 1, y, z))
							BlockTesselator.drawXNFace(x, y, z, block, getBlockLightLevel(x - 1, y, z) / 15f);
						if (isBlockTransparent(x, y - 1, z))
							BlockTesselator.drawYNFace(x, y, z, block, getBlockLightLevel(x, y - 1, z) / 15f);
					}
				}
		
		GL11.glEnd();
		GL11.glEndList();
		
		chunk.needsGeometryRebuild = false;
	}
	
	private static void generateChunkGeometryWithAmbiantOcclusion(Chunk chunk)
	{
		GL11.glNewList(chunk.displayListId, GL11.GL_COMPILE);
		GL11.glBegin(GL11.GL_QUADS);
		
		int xMax = chunk.offset.x + Chunk.WIDTH;
		int zMax = chunk.offset.z + Chunk.WIDTH;
		
		for (int x = chunk.offset.x; x < xMax; x++)
			for (int y = 0; y < Chunk.HEIGHT; y++)
				for (int z = chunk.offset.z; z < zMax; z++)
				{
					int id = getBlockId(x, y, z);
					if (!isBlockAir(x, y, z))
					{
						Block block = Block.getBlockFromId(id);
						
						if (Block.shouldRenderFace(id, getBlockId(x, y + 1, z)))
							BlockTesselator.drawYPFaceWithAmbiantOcclusion(x, y, z, block, getBlockLightLevel(x, y + 1, z), getBlockLightLevel(x, y + 1, z + 1), getBlockLightLevel(x + 1, y + 1, z + 1), getBlockLightLevel(x + 1, y + 1, z), getBlockLightLevel(x + 1, y + 1, z - 1), getBlockLightLevel(x, y + 1, z - 1), getBlockLightLevel(x - 1, y + 1, z - 1), getBlockLightLevel(x - 1, y + 1, z), getBlockLightLevel(x - 1, y + 1, z + 1));
						if (Block.shouldRenderFace(id, getBlockId(x, y, z + 1)))
							BlockTesselator.drawZPFaceWithAmbiantOcclusion(x, y, z, block, getBlockLightLevel(x, y, z + 1), getBlockLightLevel(x, y - 1, z + 1), getBlockLightLevel(x + 1, y - 1, z + 1), getBlockLightLevel(x + 1, y, z + 1), getBlockLightLevel(x + 1, y + 1, z + 1), getBlockLightLevel(x, y + 1, z + 1), getBlockLightLevel(x - 1, y + 1, z + 1), getBlockLightLevel(x - 1, y, z + 1), getBlockLightLevel(x - 1, y - 1, z + 1));
						if (Block.shouldRenderFace(id, getBlockId(x + 1, y, z)))
							BlockTesselator.drawXPFaceWithAmbiantOcclusion(x, y, z, block, getBlockLightLevel(x + 1, y, z), getBlockLightLevel(x + 1, y - 1, z), getBlockLightLevel(x + 1, y - 1, z - 1), getBlockLightLevel(x + 1, y, z - 1), getBlockLightLevel(x + 1, y + 1, z - 1), getBlockLightLevel(x + 1, y + 1, z), getBlockLightLevel(x + 1, y + 1, z + 1), getBlockLightLevel(x + 1, y, z + 1), getBlockLightLevel(x + 1, y - 1, z + 1));
						if (Block.shouldRenderFace(id, getBlockId(x, y, z - 1)))
							BlockTesselator.drawZNFaceWithAmbiantOcclusion(x, y, z, block, getBlockLightLevel(x, y, z - 1), getBlockLightLevel(x, y - 1, z - 1), getBlockLightLevel(x - 1, y - 1, z - 1), getBlockLightLevel(x - 1, y, z - 1), getBlockLightLevel(x - 1, y + 1, z - 1), getBlockLightLevel(x, y + 1, z - 1), getBlockLightLevel(x + 1, y + 1, z - 1), getBlockLightLevel(x + 1, y, z - 1), getBlockLightLevel(x + 1, y - 1, z - 1));
						if (Block.shouldRenderFace(id, getBlockId(x - 1, y, z)))
							BlockTesselator.drawXNFaceWithAmbiantOcclusion(x, y, z, block, getBlockLightLevel(x - 1, y, z), getBlockLightLevel(x - 1, y - 1, z), getBlockLightLevel(x - 1, y - 1, z + 1), getBlockLightLevel(x - 1, y, z + 1), getBlockLightLevel(x - 1, y + 1, z + 1), getBlockLightLevel(x - 1, y + 1, z), getBlockLightLevel(x - 1, y + 1, z - 1), getBlockLightLevel(x - 1, y, z - 1), getBlockLightLevel(x - 1, y - 1, z - 1));
						if (Block.shouldRenderFace(id, getBlockId(x, y - 1, z)))
							BlockTesselator.drawYNFaceWithAmbiantOcclusion(x, y, z, block, getBlockLightLevel(x, y - 1, z), getBlockLightLevel(x, y - 1, z - 1), getBlockLightLevel(x + 1, y - 1, z - 1), getBlockLightLevel(x + 1, y - 1, z), getBlockLightLevel(x + 1, y - 1, z + 1), getBlockLightLevel(x, y - 1, z + 1), getBlockLightLevel(x - 1, y - 1, z + 1), getBlockLightLevel(x - 1, y - 1, z), getBlockLightLevel(x - 1, y - 1, z - 1));
					}
				}
		
		GL11.glEnd();
		GL11.glEndList();
		
		chunk.needsGeometryRebuild = false;
	}
	
	public static void setBlock(Vec3i blockPosition, int id)
	{
		setBlock(blockPosition, id, ORIENTATION_TOP_0_DEGREES);
	}
	
	public static void setBlock(Vec3i blockPosition, int id, int orientation)
	{
		setBlock(blockPosition.x, blockPosition.y, blockPosition.z, id, orientation);
	}
	
	public static void setBlock(Vec3i blockPosition, String blockName)
	{
		setBlock(blockPosition, blockName, ORIENTATION_TOP_0_DEGREES);
	}
	
	public static void setBlock(Vec3i blockPosition, String blockName, int orientation)
	{
		setBlock(blockPosition.x, blockPosition.y, blockPosition.z, blockName, orientation);
	}
	
	public static void setBlock(int x, int y, int z, String blockName)
	{
		setBlock(x, y, z, blockName, ORIENTATION_TOP_0_DEGREES);
	}
	
	public static void setBlock(int x, int y, int z, String blockName, int orientation)
	{
		setBlock(x, y, z, Block.getBlockIdFromName(blockName), orientation);
	}
	
	public static void setBlock(int x, int y, int z, int id, int orientation)
	{
		setBlockNoUpdate(x, y, z, id, orientation);
		updateBlockAndNeightboursState(x, y, z);
		propagateArtificialLightUpdate(x, y, z);
	}
	
	public static void setBlockNoUpdate(int x, int y, int z, String blockName)
	{
		setBlockNoUpdate(x, y, z, Block.getBlockIdFromName(blockName), ORIENTATION_TOP_0_DEGREES);
	}
	
	public static void setBlockNoUpdate(int x, int y, int z, String blockName, int orientation)
	{
		setBlockNoUpdate(x, y, z, Block.getBlockIdFromName(blockName), orientation);
	}
	
	public static void setBlockNoUpdate(int x, int y, int z, int id, int orientation)
	{
		setBlockId(x, y, z, id);
		setBlockOrientation(x, y, z, orientation);
	}
	
	public static Block getBlockType(Vec3i blockPosition)
	{
		return getBlockType(blockPosition.x, blockPosition.y, blockPosition.z);
	}
	
	public static Block getBlockType(int x, int y, int z)
	{
		int id = getBlockId(x, y, z);
		return Block.getBlockFromId(id);
	}
	
	public static int getBlockMetadata(Vec3i blockPosition)
	{
		return getBlockMetadata(blockPosition.x, blockPosition.y, blockPosition.z);
	}
	
	public static int getBlockMetadata(int x, int y, int z)
	{
		int coords = inline3DBlockCoords(x, y, z);
		return coords >= 0 ? blocks[coords] : 1;
	}
	
	private static void setBlockMetadata(int x, int y, int z, int metadata)
	{
		int coords = inline3DBlockCoords(x, y, z);
		if (coords >= 0)
			blocks[coords] = metadata;
	}
	
	public static int getBlockId(Vec3i blockPosition)
	{
		return getBlockId(blockPosition.x, blockPosition.y, blockPosition.z);
	}
	
	public static int getBlockId(int x, int y, int z)
	{
		int coords = inline3DBlockCoords(x, y, z);
		return coords >= 0 ? blocks[coords] & ID : coords == -2 ? 0 : 1;
	}
	
	private static void setBlockId(int x, int y, int z, int id)
	{
		int coords = inline3DBlockCoords(x, y, z);
		if (coords >= 0)
		{
			blocks[coords] &= ID_INVERSE;
			blocks[coords] |= id;
		}
	}
	
	public static int getBlockSunLightLevel(Vec3i blockPosition)
	{
		return getBlockSunLightLevel(blockPosition.x, blockPosition.y, blockPosition.z);
	}
	
	public static int getBlockSunLightLevel(int x, int y, int z)
	{
		int coords = inline3DBlockCoords(x, y, z);
		return coords >= 0 ? (blocks[coords] & SUN_LIGHT_LEVEL) >> SUN_LIGHT_LEVEL_OFFSET : 0;
	}
	
	private static void setBlockSunLightLevel(int x, int y, int z, int lightLevel)
	{
		int coords = inline3DBlockCoords(x, y, z);
		if (coords >= 0)
		{
			blocks[coords] &= SUN_LIGHT_LEVEL_INVERSE;
			blocks[coords] |= lightLevel << SUN_LIGHT_LEVEL_OFFSET;
		}
	}
	
	private static void setChunkSunLightLevel(Chunk chunk, int lightLevel)
	{
		int xStart = chunk.offset.x - centerBlock.x + negativeViewDistanceInBlocks;
		int zStart = chunk.offset.z - centerBlock.z + negativeViewDistanceInBlocks;
		
		if (xStart < mapWidthInBlocks && xStart >= 0 && zStart < mapWidthInBlocks && zStart >= 0)
		{
			xStart = (xStart + chunkShiftInBlocks.x) % mapWidthInBlocks;
			zStart = (zStart + chunkShiftInBlocks.z) % mapWidthInBlocks;
			
			int xMax = xStart + Chunk.WIDTH;
			int zMax = zStart + Chunk.WIDTH;
			
			for (int z = zStart; z < zMax; z++)
				for (int x = xStart; x < xMax; x++)
					for (int y = 0; y < Chunk.HEIGHT; y++)
					{
						int coords = x + mapWidthInBlocks * (y + Chunk.HEIGHT * z);
						
						blocks[coords] &= SUN_LIGHT_LEVEL_INVERSE;
						if (Block.getBlockFromId(blocks[coords] & ID).isTransparent())
							blocks[coords] |= lightLevel << SUN_LIGHT_LEVEL_OFFSET;
					}
		}
	}
	
	private static boolean getBlockSunLightFlag(int x, int y, int z)
	{
		int coords = inline3DBlockCoords(x, y, z);
		return coords >= 0 ? ((blocks[coords] & SUN_LIGHT_FLAG) >> SUN_LIGHT_FLAG_OFFSET) == 1 ? true : false : false;
	}
	
	private static void setBlockSunLightFlag(int x, int y, int z, boolean state)
	{
		int coords = inline3DBlockCoords(x, y, z);
		if (coords >= 0)
		{
			if (state)
			{
				blocks[coords] |= SUN_LIGHT_FLAG;
				blocks[coords] &= SUN_LIGHT_LEVEL_INVERSE;
				getChunkFromBlockCoords(x, z).needsLightingUpdate = true;
			}
			else
				blocks[coords] &= SUN_LIGHT_FLAG_INVERSE;
		}
	}
	
	public static int getBlockArtificialLightLevel(Vec3i blockPosition)
	{
		return getBlockArtificialLightLevel(blockPosition.x, blockPosition.y, blockPosition.z);
	}
	
	public static int getBlockArtificialLightLevel(int x, int y, int z)
	{
		int coords = inline3DBlockCoords(x, y, z);
		return coords >= 0 ? (blocks[coords] & ARTIFICIAL_LIGHT_LEVEL) >> ARTIFICIAL_LIGHT_LEVEL_OFFSET : 0;
	}
	
	private static void setBlockArtificialLightLevel(int x, int y, int z, int lightLevel)
	{
		int coords = inline3DBlockCoords(x, y, z);
		if (coords >= 0)
		{
			blocks[coords] &= ARTIFICIAL_LIGHT_LEVEL_INVERSE;
			blocks[coords] |= lightLevel << ARTIFICIAL_LIGHT_LEVEL_OFFSET;
		}
	}
	
	private static boolean getBlockArtificialLightFlag(int x, int y, int z)
	{
		int coords = inline3DBlockCoords(x, y, z);
		return coords >= 0 ? ((blocks[coords] & ARTIFICIAL_LIGHT_FLAG) >> ARTIFICIAL_LIGHT_FLAG_OFFSET) == 1 ? true : false : false;
	}
	
	private static void setBlockArtificialLightFlag(int x, int y, int z, boolean state)
	{
		int coords = inline3DBlockCoords(x, y, z);
		if (coords >= 0)
		{
			if (state)
			{
				blocks[coords] |= ARTIFICIAL_LIGHT_FLAG;
				blocks[coords] &= ARTIFICIAL_LIGHT_LEVEL_INVERSE;
				getChunkFromBlockCoords(x, z).needsLightingUpdate = true;
			}
			else
				blocks[coords] &= ARTIFICIAL_LIGHT_FLAG_INVERSE;
		}
	}
	
	public static int getBlockLightLevel(Vec3i blockPosition)
	{
		return getBlockArtificialLightLevel(blockPosition.x, blockPosition.y, blockPosition.z);
	}
	
	public static int getBlockLightLevel(int x, int y, int z)
	{
		int coords = inline3DBlockCoords(x, y, z);
		if (coords >= 0)
		{
			int sunLightLevel = (blocks[coords] & SUN_LIGHT_LEVEL) >> SUN_LIGHT_LEVEL_OFFSET;
			int artificialLightLevel = (blocks[coords] & ARTIFICIAL_LIGHT_LEVEL) >> ARTIFICIAL_LIGHT_LEVEL_OFFSET;
			return sunLightLevel > artificialLightLevel ? sunLightLevel : artificialLightLevel;
		}
		else
			return 15;
	}
	
	public static int getBlockOrientation(Vec3i blockPosition)
	{
		return getBlockOrientation(blockPosition.x, blockPosition.y, blockPosition.z);
	}
	
	public static int getBlockOrientation(int x, int y, int z)
	{
		int coords = inline3DBlockCoords(x, y, z);
		return coords >= 0 ? (blocks[coords] & ORIENTATION) >> ORIENTATION_OFFSET : 1;
	}
	
	public static void setBlockOrientation(int x, int y, int z, int orientation)
	{
		int coords = inline3DBlockCoords(x, y, z);
		if (coords >= 0)
		{
			blocks[coords] &= ORIENTATION_INVERSE;
			blocks[coords] |= orientation << ORIENTATION_OFFSET;
		}
	}
	
	public static int getBlockProtectionLevel(Vec3i blockPosition)
	{
		return getBlockProtectionLevel(blockPosition.x, blockPosition.y, blockPosition.z);
	}
	
	public static int getBlockProtectionLevel(int x, int y, int z)
	{
		int coords = inline3DBlockCoords(x, y, z);
		return coords >= 0 ? (blocks[coords] & PROTECTION_LEVEL) >> PROTECTION_LEVEL_OFFSET : 1;
	}
	
	public static void setBlockProtectionLevel(int x, int y, int z, int protectionLevel)
	{
		int coords = inline3DBlockCoords(x, y, z);
		if (coords >= 0)
		{
			blocks[coords] &= PROTECTION_LEVEL_INVERSE;
			blocks[coords] |= protectionLevel << PROTECTION_LEVEL_OFFSET;
		}
	}
	
	public static boolean isBlockEqualTo(int x, int y, int z, String blockName)
	{
		return isBlockEqualTo(x, y, z, Block.getBlockIdFromName(blockName));
	}
	
	public static boolean isBlockEqualTo(int x, int y, int z, int id)
	{
		return getBlockId(x, y, z) == id;
	}
	
	private static void updateBlockAndNeightboursState(int x, int y, int z)
	{
		updateBlockState(x, y, z);
		updateBlockState(x, y + 1, z);
		updateBlockState(x, y, z + 1);
		updateBlockState(x + 1, y, z);
		updateBlockState(x, y, z - 1);
		updateBlockState(x - 1, y, z);
		updateBlockState(x, y - 1, z);
	}
	
	private static void updateBlockState(int x, int y, int z)
	{
		getChunkFromBlockCoords(x, z).isModified = true;
	}
	
	public static boolean isBlockAir(int x, int y, int z)
	{
		return getBlockId(x, y, z) == 0;
	}
	
	public static boolean isBlockReal(int x, int y, int z)
	{
		return getBlockId(x, y, z) > 1;
	}
	
	private static boolean isBlockTransparent(int x, int y, int z)
	{
		return getBlockType(x, y, z).isTransparent();
	}
	
	public static Chunk getChunkFromBlockCoords(int x, int z)
	{
		x = x < 0 ? (x + 1) / Chunk.WIDTH - 1 : x / Chunk.WIDTH;
		z = z < 0 ? (z + 1) / Chunk.WIDTH - 1 : z / Chunk.WIDTH;
		
		int coords = inlineChunkCoords(x, z);
		if (coords != -1)
			return chunks[coords];
		else
			return chunkNull;
	}
	
	public static int inline3DBlockCoords(int x, int y, int z)
	{
		if (y < Chunk.HEIGHT)
		{
			if (y >= 0)
			{
				x = x - centerBlock.x + negativeViewDistanceInBlocks;
				z = z - centerBlock.z + negativeViewDistanceInBlocks;
				
				if (x < mapWidthInBlocks && x >= 0 && z < mapWidthInBlocks && z >= 0)
				{
					x = (x + chunkShiftInBlocks.x) % mapWidthInBlocks;
					z = (z + chunkShiftInBlocks.z) % mapWidthInBlocks;
					
					return x + mapWidthInBlocks * (y + Chunk.HEIGHT * z);
				}
				else
					return -1;
			}
			else
				return -1;
		}
		else
			return -2;
	}
	
	public static int inline2DBlockCoords(int x, int z)
	{
		x = x - centerBlock.x + negativeViewDistanceInBlocks;
		z = z - centerBlock.z + negativeViewDistanceInBlocks;
		
		if (x < mapWidthInBlocks && x >= 0 && z < mapWidthInBlocks && z >= 0)
		{
			x = (x + chunkShiftInBlocks.x) % mapWidthInBlocks;
			z = (z + chunkShiftInBlocks.z) % mapWidthInBlocks;
			
			return x + mapWidthInBlocks * z;
		}
		else
			return -1;
	}
	
	public static int getChunkCoordXFromBlockCoordX(int coordX)
	{
		return coordX < 0 ? (coordX + 1) / Chunk.WIDTH - 1 : coordX / Chunk.WIDTH;
	}
	
	public static int getChunkCoordZFromBlockCoordZ(int coordZ)
	{
		return coordZ < 0 ? (coordZ + 1) / Chunk.WIDTH - 1 : coordZ / Chunk.WIDTH;
	}
	
	public static int inlineChunkCoords(int x, int z)
	{
		x = x - centerChunk.x + viewDistanceInChunks;
		z = z - centerChunk.z + viewDistanceInChunks;
		
		if (x < mapWidthInChunks && x >= 0 && z < mapWidthInChunks && z >= 0)
		{
			x = (x + chunkShiftInChunks.x) % mapWidthInChunks;
			z = (z + chunkShiftInChunks.z) % mapWidthInChunks;
			
			return x + z * mapWidthInChunks;
		}
		else
			return -1;
	}
	
	public static void updatePointedBlockCoordinates()
	{
		float xzLen = (float) Math.cos(-Player.getPitch());
		float x = (float) (xzLen * Math.cos(Player.getYaw() - Math.PI / 2));
		float y = (float) Math.sin(-Player.getPitch());
		float z = (float) (xzLen * Math.sin(Player.getYaw() - Math.PI / 2));
		
		getIntersectedBlock(Player.position.x, Player.position.y, Player.position.z, x, y, z, 100);
	}
	
	public static void getIntersectedBlock(float originX, float originY, float originZ, float directionX, float directionY, float directionZ, float range)
	{
		int x = MathHelper.floor(originX);
		int y = MathHelper.floor(originY);
		int z = MathHelper.floor(originZ);
		
		float positionX = originX;
		float positionY = originY;
		float positionZ = originZ;
		
		float rangeSquared = MathHelper.square(range);
		float lengthSquared = 0;
		while (isBlockAir(x, y, z) && rangeSquared > lengthSquared)
		{
			selectedBlockForPlacing.set(x, y, z);
			
			positionX += directionX * 0.001f;
			positionY += directionY * 0.001f;
			positionZ += directionZ * 0.001f;
			
			x = MathHelper.floor(positionX);
			y = MathHelper.floor(positionY);
			z = MathHelper.floor(positionZ);
			
			lengthSquared = MathHelper.square(positionX - originX) + MathHelper.square(positionY - originY) + MathHelper.square(positionZ - originZ);
		}
		selectedBlock.set(x, y, z);
		blockSelected = isBlockReal(x, y, z);
	}
	
	public static void renderSelectedBlockBoundingBox()
	{
		if (!blockSelected)
			return;
		
		int x = selectedBlock.x;
		int y = selectedBlock.y;
		int z = selectedBlock.z;
		
		GL11.glBegin(GL11.GL_LINE_STRIP);
		
		GL20.glVertexAttrib2f(1, 2, 2);
		GL20.glVertexAttrib3f(0, x - 0.005f, y - 0.005f, z + 1.01f);
		GL20.glVertexAttrib2f(1, 2, 2);
		GL20.glVertexAttrib3f(0, x + 1.01f, y - 0.005f, z + 1.01f);
		GL20.glVertexAttrib2f(1, 2, 2);
		GL20.glVertexAttrib3f(0, x + 1.01f, y + 1.01f, z + 1.01f);
		GL20.glVertexAttrib2f(1, 2, 2);
		GL20.glVertexAttrib3f(0, x - 0.005f, y + 1.01f, z + 1.01f);
		GL20.glVertexAttrib2f(1, 2, 2);
		GL20.glVertexAttrib3f(0, x - 0.005f, y - 0.005f, z + 1.01f);
		
		GL20.glVertexAttrib2f(1, 2, 2);
		GL20.glVertexAttrib3f(0, x + 1.01f, y - 0.005f, z + 1.01f);
		GL20.glVertexAttrib2f(1, 2, 2);
		GL20.glVertexAttrib3f(0, x + 1.01f, y - 0.005f, z - 0.005f);
		GL20.glVertexAttrib2f(1, 2, 2);
		GL20.glVertexAttrib3f(0, x + 1.01f, y + 1.01f, z - 0.005f);
		GL20.glVertexAttrib2f(1, 2, 2);
		GL20.glVertexAttrib3f(0, x + 1.01f, y + 1.01f, z + 1.01f);
		GL20.glVertexAttrib2f(1, 2, 2);
		GL20.glVertexAttrib3f(0, x + 1.01f, y - 0.005f, z + 1.01f);
		
		GL20.glVertexAttrib2f(1, 2, 2);
		GL20.glVertexAttrib3f(0, x + 1.01f, y - 0.005f, z - 0.005f);
		GL20.glVertexAttrib2f(1, 2, 2);
		GL20.glVertexAttrib3f(0, x - 0.005f, y - 0.005f, z - 0.005f);
		GL20.glVertexAttrib2f(1, 2, 2);
		GL20.glVertexAttrib3f(0, x - 0.005f, y + 1.01f, z - 0.005f);
		GL20.glVertexAttrib2f(1, 2, 2);
		GL20.glVertexAttrib3f(0, x + 1.01f, y + 1.01f, z - 0.005f);
		GL20.glVertexAttrib2f(1, 2, 2);
		GL20.glVertexAttrib3f(0, x + 1.01f, y - 0.005f, z - 0.005f);
		
		GL20.glVertexAttrib2f(1, 2, 2);
		GL20.glVertexAttrib3f(0, x - 0.005f, y - 0.005f, z - 0.005f);
		GL20.glVertexAttrib2f(1, 2, 2);
		GL20.glVertexAttrib3f(0, x - 0.005f, y - 0.005f, z + 1.01f);
		GL20.glVertexAttrib2f(1, 2, 2);
		GL20.glVertexAttrib3f(0, x - 0.005f, y + 1.01f, z + 1.01f);
		GL20.glVertexAttrib2f(1, 2, 2);
		GL20.glVertexAttrib3f(0, x - 0.005f, y + 1.01f, z - 0.005f);
		GL20.glVertexAttrib2f(1, 2, 2);
		GL20.glVertexAttrib3f(0, x - 0.005f, y - 0.005f, z - 0.005f);
		
		GL11.glEnd();
	}
}