package game.world;

import java.util.Random;

public class WorldGenerator
{
	private static int seed = 0x51a5eba;
	private static Random random = new Random(seed);
	
	public static void generateChunk(int x, int z)
	{
		int xMin = x * Chunk.WIDTH;
		int zMin = z * Chunk.WIDTH;
		int xMax = xMin + Chunk.WIDTH;
		int zMax = zMin + Chunk.WIDTH;
		
		if (x > 0 && false)
		{
			for (x = xMin; x < xMax; x++)
				for (z = zMin; z < zMax; z++)
					for (int y = 0; y < 256; y++)
						BlockManager.setBlockNoUpdate(x, y, z, "air");
			return;
		}
		
		for (x = xMin; x < xMax; x++)
			for (z = zMin; z < zMax; z++)
			{
				double noise = SimplexNoise.noise(x, z) * 20 + 100;
				for (int y = 0; y < Chunk.HEIGHT; y++)
				{
					if (y < noise - 5)
						BlockManager.setBlockNoUpdate(x, y, z, "stone");
					
					else if (y < noise - 1)
						BlockManager.setBlockNoUpdate(x, y, z, "dirt");
					else if (y < noise)
					{
						BlockManager.setBlockNoUpdate(x, y, z, "grass");
					}
				}
			}
		for (x = xMin; x < xMax; x++)
			for (z = zMin; z < zMax; z++)
			{
				generateBedrock(x, z);
				int noise = (int) (SimplexNoise.noise(x, z) * 20 + 100);
				
				for (int y = 0; y < Chunk.HEIGHT; y++)
				{
					if (y < noise - 80 && y > 5)
					{
						if (oneChanceOn(2000))
							generateVein(x, y, z, "diamond_ore");
						else if (oneChanceOn(1500))
							generateVein(x, y, z, "emerald_ore");
						else if (oneChanceOn(1000))
							generateVein(x, y, z, "lapis_ore");
						else if (oneChanceOn(800))
							generateVein(x, y, z, "redstone_ore");
						else if (oneChanceOn(1000))
							generateVein(x, y, z, "gold_ore");
						else if (oneChanceOn(150))
							generateVein(x, y, z, "coal_ore");
						else if (oneChanceOn(200))
							generateVein(x, y, z, "iron_ore");
						
					}
					else if (y < noise - 60 && y > 5)
					{
						if (oneChanceOn(1500))
							generateVein(x, y, z, "emerald_ore");
						else if (oneChanceOn(1000))
							generateVein(x, y, z, "lapis_ore");
						else if (oneChanceOn(800))
							generateVein(x, y, z, "redstone_ore");
						else if (oneChanceOn(1000))
							generateVein(x, y, z, "gold_ore");
						else if (oneChanceOn(150))
							generateVein(x, y, z, "coal_ore");
						else if (oneChanceOn(200))
							generateVein(x, y, z, "iron_ore");
					}
					else if (y < noise - 40 && y > 5)
					{
						if (oneChanceOn(1000))
							generateVein(x, y, z, "lapis_ore");
						else if (oneChanceOn(800))
							generateVein(x, y, z, "redstone_ore");
						else if (oneChanceOn(1000))
							generateVein(x, y, z, "gold_ore");
						else if (oneChanceOn(150))
							generateVein(x, y, z, "coal_ore");
						else if (oneChanceOn(200))
							generateVein(x, y, z, "iron_ore");
					}
					else if (y < noise - 8 && y > 5)
					{
						if (oneChanceOn(150))
							generateVein(x, y, z, "coal_ore");
						else if (oneChanceOn(200))
							generateVein(x, y, z, "iron_ore");
					}
					
				}
				
			}
		for (x = xMin; x < xMax; x++)
			for (z = zMin; z < zMax; z++)
				for (int y = 0; y < Chunk.HEIGHT; y++)
				
				{
					int noise = (int) (SimplexNoise.noise(x, y, z) * 100);
					if (noise > 90)
						BlockManager.setBlockNoUpdate(x, y, z, 0, 0);
				}
		
		for (x = xMin; x < xMax; x++)
			for (z = zMin; z < zMax; z++)
			{
				int noise = (int) (SimplexNoise.noise(x, z) * 20 + 100);
				if (oneChanceOn(1200))
					generateRock(x, noise + 1, z);
				else if (oneChanceOn(120))
					generateTree(x, noise + 1, z);
			}
		
	}
	
	public static void generateTree(int x, int y, int z)
	{
		if (!BlockManager.isBlockEqualTo(x, y - 1, z, "grass"))
			return;
		
		int nb = random(0, 4);
		if (nb < 4)
		{
			BlockManager.setBlockNoUpdate(x, y, z, "log_oak");
			BlockManager.setBlockNoUpdate(x, y + 1, z, "log_oak");
			BlockManager.setBlockNoUpdate(x, y + 2, z, "log_oak");
			BlockManager.setBlockNoUpdate(x, y + 3, z, "log_oak");
			
			BlockManager.setBlockNoUpdate(x + 1, y + 2, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 1, y + 2, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 2, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 2, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 2, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 2, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 2, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 1, y + 2, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 2, y + 2, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 2, y + 2, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 2, y + 2, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 2, y + 2, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 2, y + 2, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 2, y + 2, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 1, y + 2, z + 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 2, z + 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 2, z + 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 1, y + 2, z - 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 2, z - 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 2, z - 2, "leaves_oak");
			
			BlockManager.setBlockNoUpdate(x + 1, y + 3, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 1, y + 3, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 3, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 3, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 3, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 3, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 3, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 1, y + 3, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 2, y + 3, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 2, y + 3, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 2, y + 3, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 2, y + 3, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 2, y + 3, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 2, y + 3, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 1, y + 3, z + 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 3, z + 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 3, z + 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 1, y + 3, z - 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 3, z - 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 3, z - 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 2, y + 3, z + 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 2, y + 3, z - 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 2, y + 3, z + 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 2, y + 3, z - 2, "leaves_oak");
			
			BlockManager.setBlockNoUpdate(x + 1, y + 4, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 1, y + 4, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 4, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 4, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 4, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 4, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 4, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 1, y + 4, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 4, z, "leaves_oak");
			
			BlockManager.setBlockNoUpdate(x, y + 5, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 1, y + 5, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 5, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 5, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 5, z - 1, "leaves_oak");
		}
		else
		{
			BlockManager.setBlockNoUpdate(x, y, z, "log_birch");
			BlockManager.setBlockNoUpdate(x, y + 1, z, "log_birch");
			BlockManager.setBlockNoUpdate(x, y + 2, z, "log_birch");
			BlockManager.setBlockNoUpdate(x, y + 3, z, "log_birch");
			
			BlockManager.setBlockNoUpdate(x + 1, y + 2, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 1, y + 2, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 2, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 2, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 2, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 2, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 2, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 1, y + 2, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 2, y + 2, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 2, y + 2, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 2, y + 2, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 2, y + 2, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 2, y + 2, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 2, y + 2, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 1, y + 2, z + 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 2, z + 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 2, z + 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 1, y + 2, z - 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 2, z - 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 2, z - 2, "leaves_oak");
			
			BlockManager.setBlockNoUpdate(x + 1, y + 3, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 1, y + 3, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 3, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 3, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 3, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 3, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 3, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 1, y + 3, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 2, y + 3, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 2, y + 3, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 2, y + 3, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 2, y + 3, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 2, y + 3, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 2, y + 3, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 1, y + 3, z + 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 3, z + 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 3, z + 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 1, y + 3, z - 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 3, z - 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 3, z - 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 2, y + 3, z + 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 2, y + 3, z - 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 2, y + 3, z + 2, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 2, y + 3, z - 2, "leaves_oak");
			
			BlockManager.setBlockNoUpdate(x + 1, y + 4, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 1, y + 4, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 4, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 4, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 4, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 4, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 4, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 1, y + 4, z - 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 4, z, "leaves_oak");
			
			BlockManager.setBlockNoUpdate(x, y + 5, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x + 1, y + 5, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x - 1, y + 5, z, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 5, z + 1, "leaves_oak");
			BlockManager.setBlockNoUpdate(x, y + 5, z - 1, "leaves_oak");
		}
	}
	
	public static void generateRock(int x, int y, int z)
	{
		if (!BlockManager.isBlockEqualTo(x, y - 1, z, "grass"))
			return;
		
		BlockManager.setBlockNoUpdate(x, y - 1, z, "stone");
		BlockManager.setBlockNoUpdate(x + 1, y - 1, z + 1, "stone");
		BlockManager.setBlockNoUpdate(x - 1, y - 1, z + 1, "stone");
		BlockManager.setBlockNoUpdate(x - 1, y - 1, z - 1, "stone");
		BlockManager.setBlockNoUpdate(x + 1, y - 1, z - 1, "stone");
		
		BlockManager.setBlockNoUpdate(x, y, z, "stone");
		BlockManager.setBlockNoUpdate(x, y, z + 1, "stone");
		BlockManager.setBlockNoUpdate(x, y, z - 1, "stone");
		BlockManager.setBlockNoUpdate(x + 1, y, z + 1, "stone");
		BlockManager.setBlockNoUpdate(x + 1, y, z - 1, "stone");
		BlockManager.setBlockNoUpdate(x + 1, y, z, "stone");
		BlockManager.setBlockNoUpdate(x - 1, y, z, "stone");
		BlockManager.setBlockNoUpdate(x - 1, y, z + 1, "stone");
		BlockManager.setBlockNoUpdate(x - 1, y, z - 1, "stone");
		
		BlockManager.setBlockNoUpdate(x, y + 1, z, "stone");
		BlockManager.setBlockNoUpdate(x - 1, y + 1, z, "stone");
		BlockManager.setBlockNoUpdate(x, y + 1, z - 1, "stone");
		BlockManager.setBlockNoUpdate(x + 1, y + 1, z, "stone");
		BlockManager.setBlockNoUpdate(x + 1, y + 1, z + 1, "stone");
		BlockManager.setBlockNoUpdate(x, y + 1, z + 1, "stone");
		
		BlockManager.setBlockNoUpdate(x + 1, y + 2, z, "stone");
		BlockManager.setBlockNoUpdate(x, y + 2, z, "stone");
		BlockManager.setBlockNoUpdate(x, y + 2, z + 1, "stone");
		
		BlockManager.setBlockNoUpdate(x, y + 3, z, "stone");
	}
	
	public static void generateVein(int x, int y, int z, String blockName)
	{
		int nb = random(8, 15);
		BlockManager.setBlockNoUpdate(x, y, z, blockName);
		for (int cpt = 1; cpt < nb; cpt++)
		{
			int i = random(1, 3);
			if (i == 1)
				x += random(-1, 1);
			else if (i == 2)
				y += random(-1, 1);
			else
				z += random(-1, 1);
			BlockManager.setBlockNoUpdate(x, y, z, blockName);
		}
	}
	
	public static void generateBedrock(int x, int z)
	{
		BlockManager.setBlockNoUpdate(x, 0, z, "bedrock");
		int y = random(0, 1);
		if (y == 1)
		{
			BlockManager.setBlockNoUpdate(x, 1, z, "bedrock");
			y = random(0, 1);
			if (y == 1)
				BlockManager.setBlockNoUpdate(x, 2, z, "bedrock");
		}
		
	}
	
	public static int random(int min, int max)
	{
		return random.nextInt(max + 1 - min) + min;
	}
	
	public static boolean oneChanceOn(int i)
	{
		return random(0, i) == 0;
	}
}
