package game.world;

import java.util.Random;

public class PerlinNoise
{
	public static float[][] generateWhiteNoise(int width, int seed)
	{
		Random random = new Random(seed);
		float[][] noise = new float[width][width];
		
		for (int i = 0; i < width; i++)
			for (int j = 0; j < width; j++)
				noise[i][j] = (float) random.nextFloat() % 1;
		
		return noise;
	}
	
	public static float[][] generateSmoothNoise(float[][] baseNoise, int octave)
	{
		int width = baseNoise.length;
		
		float[][] smoothNoise = new float[width][width];
		
		int samplePeriod = 1 << octave; // calculates 2 ^ k
		float sampleFrequency = 1.0f / samplePeriod;
		
		for (int i = 0; i < width; i++)
		{
			// calculate the horizontal sampling indices
			int sample_i0 = (i / samplePeriod) * samplePeriod;
			int sample_i1 = (sample_i0 + samplePeriod) % width; // wrap around
			float horizontal_blend = (i - sample_i0) * sampleFrequency;
			
			for (int j = 0; j < width; j++)
			{
				// calculate the vertical sampling indices
				int sample_j0 = (j / samplePeriod) * samplePeriod;
				int sample_j1 = (sample_j0 + samplePeriod) % width; // wrap
																	// around
				float vertical_blend = (j - sample_j0) * sampleFrequency;
				
				// blend the top two corners
				float top = interpolate(baseNoise[sample_i0][sample_j0], baseNoise[sample_i1][sample_j0], horizontal_blend);
				
				// blend the bottom two corners
				float bottom = interpolate(baseNoise[sample_i0][sample_j1], baseNoise[sample_i1][sample_j1], horizontal_blend);
				
				// final blend
				smoothNoise[i][j] = interpolate(top, bottom, vertical_blend);
			}
		}
		
		return smoothNoise;
	}
	
	static float[][] generatePerlinNoise(float[][] baseNoise, int octaveCount)
	{
		int width = baseNoise.length;
		
		float[][][] smoothNoise = new float[octaveCount][][]; // an array of 2D
																// arrays
																// containing
		
		float persistance = 0.5f;
		
		// generate smooth noise
		for (int i = 0; i < octaveCount; i++)
			smoothNoise[i] = generateSmoothNoise(baseNoise, i);
		
		float[][] perlinNoise = new float[width][width];
		float amplitude = 1.0f;
		float totalAmplitude = 0.0f;
		
		// blend noise together
		for (int octave = octaveCount - 1; octave >= 0; octave--)
		{
			amplitude *= persistance;
			totalAmplitude += amplitude;
			
			for (int i = 0; i < width; i++)
				for (int j = 0; j < width; j++)
					perlinNoise[i][j] += smoothNoise[octave][i][j] * amplitude;
		}
		
		// normalisation
		for (int i = 0; i < width; i++)
			for (int j = 0; j < width; j++)
				perlinNoise[i][j] /= totalAmplitude;
		
		return perlinNoise;
	}
	
	public static float interpolate(float x0, float x1, float alpha)
	{
		return x0 * (1 - alpha) + alpha * x1;
	}
}
