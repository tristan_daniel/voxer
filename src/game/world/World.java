package game.world;

import game.shaders.Sky;
import util.math.MathHelper;

public class World
{
	public static String name = "world";
	
	/*
	 * Midnight = 0, Noon = 12000
	 */
	public static final int DAY_BEGINNING = 0;
	public static final int DAY_END = 2400000;
	private static int time = 800000;
	private static int timeSpeed = 2000;
	
	public static int getTime()
	{
		return (int) (time / 2400000 * MathHelper.PI * 2);
	}
	
	public static void update(float delta)
	{
		time += delta * timeSpeed;
		
		Sky.update();
	}
}
