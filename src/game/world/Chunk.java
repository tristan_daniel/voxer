package game.world;

import org.lwjgl.opengl.GL11;

import util.math.Vec2i;

public class Chunk
{
	public static final int WIDTH = 16;
	public static final int HEIGHT = 256;
	
	public Vec2i position = new Vec2i(0, 0);
	public Vec2i offset = new Vec2i(0, 0);
	
	public int displayListId = 0;
	
	public boolean isModified = true;
	public boolean isBeingLit = false;
	public boolean needsGeometryRebuild = false;
	public boolean needsLightingUpdate = false;
	private boolean needsLoading = false;
	
	public Chunk()
	{
		displayListId = GL11.glGenLists(1);
	}
	
	public void update()
	{
		if (needsLoading)
		{
			WorldGenerator.generateChunk(position.x, position.z);
			needsGeometryRebuild = true;
			needsLoading = false;
		}
	}
	
	public void load(int x, int z)
	{
		position.set(x, z);
		offset.set(x * WIDTH, z * WIDTH);
		needsLoading = true;
	}
}
