package game.world;

public class ChunkNull extends Chunk
{
	public boolean isModified = true;
	public boolean isBeingLit = false;
	public boolean needsGeometryRebuild = false;
	public boolean needsLightingUpdate = false;
	private boolean needsLoading = false;
	
	public ChunkNull()
	{
	}
	
	public void update()
	{
	}
	
	public void load(int x, int z)
	{
	}
}
