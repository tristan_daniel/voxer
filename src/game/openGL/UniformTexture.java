package game.openGL;

import org.lwjgl.opengl.GL11;

public class UniformTexture extends Uniform
{
	private int textureId;
	private int textureType;
	
	public UniformTexture(ShaderProgram shaderProgram, String name, Texture texture)
	{
		super(shaderProgram, name);
		textureType = texture.getType();
		textureId = texture.getId();
	}
	
	public void bindOnUnit(int unit)
	{
		Texture.activeTextureUnit(unit);
		GL11.glBindTexture(textureType, textureId);
		super.upload(unit);
	}
}
