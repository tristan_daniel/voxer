package game.openGL;

import org.lwjgl.opengl.GL20;

import util.math.Vec3f;

public class Uniform
{
	protected int id = 0;
	
	public Uniform(ShaderProgram shaderProgram, String name)
	{
		id = GL20.glGetUniformLocation(shaderProgram.getId(), name);
	}
	
	public void upload(float f)
	{
		GL20.glUniform1f(id, f);
	}
	
	public void upload(int i)
	{
		GL20.glUniform1i(id, i);
	}
	
	public void upload(Vec3f vec)
	{
		GL20.glUniform3f(id, vec.x, vec.y, vec.z);
	}
}
