package game.openGL;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL30;

import start.Voxer;
import de.matthiasmann.twl.utils.PNGDecoder;
import de.matthiasmann.twl.utils.PNGDecoder.Format;

public class TextureArray extends Texture
{
	private int textureNumber;
	private int textureWidth;
	private ByteBuffer buffer;
	
	public TextureArray(int textureNumber, int textureWidth)
	{
		super();
		type = GL30.GL_TEXTURE_2D_ARRAY;
		buffer = ByteBuffer.allocateDirect(4 * textureWidth * textureWidth * textureNumber);
		this.textureNumber = textureNumber;
		this.textureWidth = textureWidth;
	}
	
	public void load(String filename)
	{
		try
		{
			InputStream in = new FileInputStream(filename);
			PNGDecoder decoder = new PNGDecoder(in);
			
			if (decoder.getWidth() != textureWidth || decoder.getHeight() != textureWidth)
				return;
			
			decoder.decode(buffer, textureWidth * 4, Format.RGBA);
			
			in.close();
		}
		catch (IOException e)
		{
			Voxer.error("Texture loading : " + e.getMessage());
		}
	}
	
	public void createTexture()
	{
		buffer.flip();
		bind();
		
		GL12.glTexImage3D(GL30.GL_TEXTURE_2D_ARRAY, 0, GL11.GL_RGBA, textureWidth, textureWidth, textureNumber, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer);
		
		GL11.glTexParameteri(GL30.GL_TEXTURE_2D_ARRAY, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
		GL11.glTexParameteri(GL30.GL_TEXTURE_2D_ARRAY, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		
		unbind();
	}
	
	public int getId()
	{
		return id;
	}
}
