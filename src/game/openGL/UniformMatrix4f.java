package game.openGL;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL20;

import util.math.Mat4f;

public class UniformMatrix4f extends Uniform
{
	private FloatBuffer buffer = BufferUtils.createFloatBuffer(16);
	
	public UniformMatrix4f(ShaderProgram shaderProgram, String name)
	{
		super(shaderProgram, name);
	}
	
	public void upload(Mat4f matrix)
	{
		matrix.store(buffer);
		buffer.flip();
		GL20.glUniformMatrix4(id, false, buffer);
	}
}
