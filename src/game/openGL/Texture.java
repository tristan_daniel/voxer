package game.openGL;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;

import start.Voxer;
import de.matthiasmann.twl.utils.PNGDecoder;
import de.matthiasmann.twl.utils.PNGDecoder.Format;

public class Texture
{
	protected int id;
	protected int type = GL11.GL_TEXTURE_2D;
	
	public Texture()
	{
		id = GL11.glGenTextures();
	}
	
	public void load(String filename)
	{
		ByteBuffer buffer = null;
		int width = 0;
		int height = 0;
		
		try
		{
			InputStream in = new FileInputStream(filename);
			PNGDecoder decoder = new PNGDecoder(in);
			
			width = decoder.getWidth();
			height = decoder.getHeight();
			
			buffer = ByteBuffer.allocateDirect(4 * width * height);
			decoder.decode(buffer, width * 4, Format.RGBA);
			buffer.flip();
			
			in.close();
		}
		catch (IOException e)
		{
			Voxer.error("Texture loading : " + e.getMessage());
		}
		
		GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 1);
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, width, height, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer);
	}
	
	public void allocateMemory(int width, int height, int component)
	{
		GL11.glTexImage2D(type, 0, component, width, height, 0, component, GL11.GL_UNSIGNED_BYTE, (ByteBuffer) null);
	}
	
	public void setParameter(int parameterName, int parameterValue)
	{
		GL11.glTexParameteri(type, parameterName, parameterValue);
	}
	
	public void destroy()
	{
		GL11.glDeleteTextures(id);
	}
	
	public int getId()
	{
		return id;
	}
	
	public int getType()
	{
		return type;
	}
	
	public void bind()
	{
		GL11.glBindTexture(type, id);
	}
	
	public void unbind()
	{
		GL11.glBindTexture(type, 0);
	}
	
	public static void activeTextureUnit(int unit)
	{
		GL13.glActiveTexture(GL13.GL_TEXTURE0 + unit);
	}
}
