package game.openGL;

import org.lwjgl.opengl.GL20;

import util.math.Vec3f;

public class UniformVec3f extends Uniform
{
	
	public UniformVec3f(ShaderProgram shaderProgram, String name)
	{
		super(shaderProgram, name);
	}
	
	public void upload(Vec3f vec)
	{
		GL20.glUniform3f(id, vec.x, vec.y, vec.z);
	}
	
}
