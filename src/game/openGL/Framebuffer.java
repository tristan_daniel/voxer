package game.openGL;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;

public class Framebuffer
{
	private int id;
	
	public Framebuffer()
	{
		id = GL30.glGenFramebuffers();
	}
	
	public void setDepthAttachment(Texture texture)
	{
		GL30.glFramebufferTexture2D(GL30.GL_FRAMEBUFFER, GL30.GL_DEPTH_ATTACHMENT, texture.getType(), texture.getId(), 0);
	}
	
	public void startDrawingOn(int width, int height)
	{
		bind();
		GL11.glViewport(0, 0, width, height);
	}
	
	public void stopDrawingOn()
	{
		unbind();
		GL11.glViewport(0, 0, Display.getWidth(), Display.getHeight());
	}
	
	public void bind()
	{
		GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, id);
	}
	
	public void unbind()
	{
		GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0);
	}
	
	public void destroy()
	{
		GL30.glDeleteFramebuffers(id);
	}
}
