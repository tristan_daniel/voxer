package game.openGL;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import start.Voxer;

public class Shader
{
	private int id = 0;
	
	public static final int VERTEX_SHADER = GL20.GL_VERTEX_SHADER;
	public static final int FRAGMENT_SHADER = GL20.GL_FRAGMENT_SHADER;
	
	public Shader(String sourceFile, int type)
	{
		String source = "";
		try
		{
			source = StandardCharsets.UTF_8.decode(ByteBuffer.wrap(Files.readAllBytes(Paths.get(sourceFile)))).toString();
		}
		catch (IOException e)
		{
			Voxer.error("Loading shader file '" + sourceFile + "': " + e.getMessage());
		}
		
		this.id = GL20.glCreateShader(type);
		GL20.glShaderSource(this.id, source);
		GL20.glCompileShader(this.id);
		
		if (GL20.glGetShaderi(id, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE)
		{
			Voxer.error("Shader compilation : " + sourceFile);
		}
	}
	
	public int getId()
	{
		return this.id;
	}
}
