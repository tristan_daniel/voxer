package game.openGL;

import org.lwjgl.opengl.GL20;

public class ShaderProgram
{
	private int id = 0;
	
	public ShaderProgram()
	{
		id = GL20.glCreateProgram();
	}
	
	public void attachShader(Shader shader)
	{
		GL20.glAttachShader(id, shader.getId());
	}
	
	public void linkAndValidate()
	{
		GL20.glLinkProgram(id);
		GL20.glValidateProgram(id);
	}
	
	public void bindAttribLocation(int index, String name)
	{
		GL20.glBindAttribLocation(id, index, name);
	}
	
	public void bind()
	{
		GL20.glUseProgram(id);
	}
	
	public void unbind()
	{
		GL20.glUseProgram(0);
	}
	
	public void destroy()
	{
		GL20.glDeleteProgram(id);
	}
	
	public int getId()
	{
		return id;
	}
}
