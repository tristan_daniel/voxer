package start;

import util.time.Time;
import activity.Activity;

public class Logic implements Runnable
{
	private static long lastUpdate = 0;
	private static float updateMinimumLength = 10f;
	private static float delta = 0;
	
	@Override
	public void run()
	{
		while (Voxer.isRunning())
		{
			updateTiming();
			
			Activity.updateActiveActivity(delta);
		}
	}
	
	private static void updateTiming()
	{
		long time = Time.getTime();
		long updateTime = (time - lastUpdate);
		delta = updateTime / 1000.0f;
		
		if (updateTime < updateMinimumLength)
			try
			{
				Thread.sleep((int) (updateMinimumLength - updateTime));
			}
			catch (InterruptedException e)
			{
				Voxer.error(e);
			}
		
		lastUpdate = time;
	}
}