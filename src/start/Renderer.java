package start;

import game.player.Player;
import game.world.BlockManager;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;

import util.math.MathHelper;
import util.time.Time;
import activity.Activity;

public class Renderer
{
	private static final String MINIMUM_VERSION = "3.0.0";
	
	private static boolean fullScreen = false;
	private static int displayWidth = 1500;
	private static int displayHeight = 800;
	
	private static boolean vSync = false;
	private static int targetFrameRate = 0;
	
	private static long lastSecond = 0;
	private static int frameRate = 0;
	private static long lastFrame = 0;
	private static float delta = 0;
	
	public static void run()
	{
		while (Voxer.isRunning())
			render();
		
		Activity.destroyActivities();
	}
	
	public static void render()
	{
		if (Display.isCloseRequested())
			Voxer.stop();
		
		Activity.renderActiveActivity(delta);
		
		updateTiming();
		Display.update();
	}
	
	public static void initOpenGL()
	{
		try
		{
			if (fullScreen)
				Display.setDisplayModeAndFullscreen(Display.getDesktopDisplayMode());
			else
				Display.setDisplayMode(new DisplayMode(displayWidth, displayHeight));
			
			Display.create();
		}
		catch (LWJGLException e)
		{
			Voxer.error("LWJGL initialization : " + e.getMessage());
		}
		
		Voxer.debug("Your computer supports OpenGL version " + GL11.glGetString(GL11.GL_VERSION) + " (the minimum required is " + MINIMUM_VERSION + ")");
		
		if (!GLContext.getCapabilities().OpenGL30)
		{
			Voxer.error("Your graphics card or driver only support OpenGL " + GL11.glGetString(GL11.GL_VERSION) + ". The minimum version required is " + MINIMUM_VERSION + ". Please update your driver or hardware to play.");
			Voxer.stop();
		}
		Display.setVSyncEnabled(vSync);
		
		GL11.glClearColor(0.459f, 0.671f, 0.851f, 0f);
		
		lastSecond = Time.getTime();
	}
	
	private static void updateTiming()
	{
		long time = Time.getTime();
		delta = (time - lastFrame) / 1000.0f;
		lastFrame = time;
		
		if (time - lastSecond > 1000)
		{
			Display.setTitle("FPS : " + frameRate + " | Memory consumption : " + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1048576 + " Mo | Lighting : " + BlockManager.getBlockLightLevel(MathHelper.floor(Player.position.x), (int) Player.position.y, MathHelper.floor(Player.position.z)));
			frameRate = 0;
			lastSecond += 1000;
		}
		frameRate++;
		
		if (!vSync && targetFrameRate > 0)
			Display.sync(targetFrameRate);
	}
}