package start;

import org.fusesource.jansi.AnsiConsole;

public class Start
{
	public static void main(String[] args)
	{
		AnsiConsole.systemInstall();
		Voxer.start();
	}
}