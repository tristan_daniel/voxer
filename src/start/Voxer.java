package start;

import activity.Activity;

public class Voxer
{
	private static boolean running = false;
	private static boolean debug = true;
	
	public static void start()
	{
		if (running)
			return;
		
		running = true;
		
		Thread.currentThread().setName("Renderer");
		Renderer.initOpenGL();
		
		Activity.createActivities();
		
		Thread logicThread = new Thread(new Logic());
		logicThread.setName("Logic");
		logicThread.start();
		
		Renderer.run();
	}
	
	public static void stop()
	{
		if (!running)
			return;
		
		running = false;
	}
	
	public static boolean isRunning()
	{
		return running;
	}
	
	public static void log(Object o)
	{
		System.out.println("\u001B[36m[Info]\u001B[0m " + o);
	}
	
	public static void warning(Object o)
	{
		System.out.println("\u001B[33m[Warning]\u001B[0m " + o);
	}
	
	public static void error(Object o)
	{
		System.out.println("\u001B[31m[Error]\u001B[0m Thread " + Thread.currentThread().getName() + ": " + o);
	}
	
	public static void debug(Object o)
	{
		if (debug)
			System.out.println("\u001B[35m[Debug]\u001B[0m " + o);
	}
}