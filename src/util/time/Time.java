package util.time;

import org.lwjgl.Sys;

public class Time
{
	public static long getTime()
	{
		return Sys.getTime() * 1000 / Sys.getTimerResolution();
	}
}
