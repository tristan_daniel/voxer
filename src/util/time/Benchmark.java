package util.time;

import start.Voxer;

public class Benchmark
{
	private static long start = 0;
	
	public static void start()
	{
		Benchmark.start = System.nanoTime();
	}
	
	public static void displayTime(String message)
	{
		long end = System.nanoTime();
		Voxer.debug(message + ((end - start) * 1e-6) + " milliseconds");
	}
	
	public static void displayTimeIfGreaterThan(long nanoSeconds, String message)
	{
		long end = System.nanoTime();
		long time = end - start;
		if (time > nanoSeconds)
			Voxer.debug(message + (time * 1e-6) + " milliseconds");
	}
}
