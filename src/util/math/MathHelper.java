package util.math;

public class MathHelper
{
	public static final float PI = (float) Math.PI;
	
	public static int max(int a, int b)
	{
		return a > b ? a : b;
	}
	
	public static int pow(int i, int power)
	{
		int j = i;
		for (int k = 1; k < power; k++)
			j *= i;
		return j;
	}
	
	public static int floor(float f)
	{
		return f < 0 ? (int) f - 1 : (int) f;
	}
	
	public static float coTangent(float angle)
	{
		return (float) (1f / Math.tan(angle));
	}
	
	public static float degreesToRadians(float degrees)
	{
		return degrees * (float) (Math.PI / 180d);
	}
	
	public static int abs(int value)
	{
		return value >= 0 ? value : -value;
	}
	
	public static float abs(float value)
	{
		return value >= 0 ? value : -value;
	}
	
	public static float sin(float f)
	{
		return (float) Math.sin(f);
	}
	
	public static float cos(float f)
	{
		return (float) Math.cos(f);
	}
	
	public static float tan(float f)
	{
		return (float) Math.tan(f);
	}
	
	public static float sqrt(float f)
	{
		return (float) Math.sqrt(f);
	}
	
	public static int murmurHash3x32(String input)
	{
		byte[] data = input.getBytes();
		
		int c1 = 0xcc9e2d51;
		int c2 = 0x1b873593;
		
		int seed = 0x1a35cf1b;
		int h1 = seed;
		
		int offset = 0;
		int len = input.length();
		int roundedEnd = offset + (len & 0xfffffffc);
		
		for (int i = offset; i < roundedEnd; i += 4)
		{
			int k1 = (data[i] & 0xff) | ((data[i + 1] & 0xff) << 8) | ((data[i + 2] & 0xff) << 16) | (data[i + 3] << 24);
			k1 *= c1;
			k1 = (k1 << 15) | (k1 >>> 17);
			k1 *= c2;
			
			h1 ^= k1;
			h1 = (h1 << 13) | (h1 >>> 19);
			h1 = h1 * 5 + 0xe6546b64;
		}
		
		int k1 = 0;
		
		switch (len & 0x03)
		{
			case 3:
				k1 = (data[roundedEnd + 2] & 0xff) << 16;
				
			case 2:
				k1 |= (data[roundedEnd + 1] & 0xff) << 8;
				
			case 1:
				k1 |= data[roundedEnd] & 0xff;
				k1 *= c1;
				k1 = (k1 << 15) | (k1 >>> 17);
				k1 *= c2;
				h1 ^= k1;
		}
		
		h1 ^= len;
		
		h1 ^= h1 >>> 16;
		h1 *= 0x85ebca6b;
		h1 ^= h1 >>> 13;
		h1 *= 0xc2b2ae35;
		h1 ^= h1 >>> 16;
		h1 &= 0b00000000000000000111111111111111;
		
		return h1;
	}
	
	public static float square(float f)
	{
		return f * f;
	}
	
	public static int signum(int i)
	{
		return i > 0 ? 1 : i < 0 ? -1 : 0;
	}
}
