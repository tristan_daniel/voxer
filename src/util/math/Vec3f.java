package util.math;

public class Vec3f
{
	public static final Vec3f xAxis = new Vec3f(1, 0, 0);
	public static final Vec3f yAxis = new Vec3f(0, 1, 0);
	public static final Vec3f zAxis = new Vec3f(0, 0, 1);
	
	public float x = 0;
	public float y = 0;
	public float z = 0;
	
	public Vec3f(Vec3f vec)
	{
		x = vec.x;
		y = vec.y;
		z = vec.z;
	}
	
	public Vec3f(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public void set(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public void add(float f)
	{
		x += f;
		y += f;
		z += f;
	}
	
	public void add(Vec3f vec)
	{
		x += vec.x;
		y += vec.y;
		z += vec.z;
	}
	
	public void substract(float f)
	{
		x -= f;
		y -= f;
		z -= f;
	}
	
	public void substract(Vec3f vec)
	{
		x -= vec.x;
		y -= vec.y;
		z -= vec.z;
	}
	
	public void multiply(float f)
	{
		x *= f;
		y *= f;
		z *= f;
	}
	
	public void multiply(Vec3f vec)
	{
		x *= vec.x;
		y *= vec.y;
		z *= vec.z;
	}
	
	public void divide(float f)
	{
		x /= f;
		y /= f;
		z /= f;
	}
	
	public void divide(Vec3f vec)
	{
		x /= vec.x;
		y /= vec.y;
		z /= vec.z;
	}
	
	public float length()
	{
		return MathHelper.sqrt(lengthSquared());
	}
	
	public float lengthSquared()
	{
		return x * x + y * y + z * z;
	}
	
	public void normalise()
	{
		float length = length();
		
		x /= length;
		y /= length;
		z /= length;
	}
	
	public void cross(Vec3f vec)
	{
		set((y * vec.z) - (z * vec.y), (z * vec.x) - (x * vec.z), (x * vec.y) - (y * vec.x));
	}
	
	public String toString()
	{
		return "Vec3f [" + x + "][" + y + "][" + z + "]";
	}
}
