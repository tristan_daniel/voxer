package util.math;


public class Vec3i
{
	public int x = 0;
	public int y = 0;
	public int z = 0;
	
	public Vec3i()
	{
	}
	
	public Vec3i(int x, int y, int z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public void set(int x, int y, int z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	@Override
	public String toString()
	{
		return "Vector3i [" + x + "][" + y + "][" + z + "]";
	}
}
