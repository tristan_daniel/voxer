package util.math;

public class Vec2i
{
	public int x = 0;
	public int z = 0;
	
	public Vec2i()
	{
	}
	
	public Vec2i(int x, int z)
	{
		this.x = x;
		this.z = z;
	}
	
	public void set(int x, int z)
	{
		this.x = x;
		this.z = z;
	}
	
	@Override
	public String toString()
	{
		return "Vector2i [" + x + "][" + z + "]";
	}
}
