package util.math;

import java.nio.FloatBuffer;

public class Mat4f
{
	public float m00, m01, m02, m03;
	public float m10, m11, m12, m13;
	public float m20, m21, m22, m23;
	public float m30, m31, m32, m33;
	
	public Mat4f()
	{
		setIdentity();
	}
	
	public void set(Mat4f mat)
	{
		m00 = mat.m00;
		m01 = mat.m01;
		m02 = mat.m02;
		m03 = mat.m03;
		m10 = mat.m10;
		m11 = mat.m11;
		m12 = mat.m12;
		m13 = mat.m13;
		m20 = mat.m20;
		m21 = mat.m21;
		m22 = mat.m22;
		m23 = mat.m23;
		m30 = mat.m30;
		m31 = mat.m31;
		m32 = mat.m32;
		m33 = mat.m33;
	}
	
	public void setZero()
	{
		m00 = 0.0f;
		m01 = 0.0f;
		m02 = 0.0f;
		m03 = 0.0f;
		m10 = 0.0f;
		m11 = 0.0f;
		m12 = 0.0f;
		m13 = 0.0f;
		m20 = 0.0f;
		m21 = 0.0f;
		m22 = 0.0f;
		m23 = 0.0f;
		m30 = 0.0f;
		m31 = 0.0f;
		m32 = 0.0f;
		m33 = 0.0f;
	}
	
	public void setIdentity()
	{
		m00 = 1.0f;
		m01 = 0.0f;
		m02 = 0.0f;
		m03 = 0.0f;
		m10 = 0.0f;
		m11 = 1.0f;
		m12 = 0.0f;
		m13 = 0.0f;
		m20 = 0.0f;
		m21 = 0.0f;
		m22 = 1.0f;
		m23 = 0.0f;
		m30 = 0.0f;
		m31 = 0.0f;
		m32 = 0.0f;
		m33 = 1.0f;
	}
	
	public void add(Mat4f mat)
	{
		m00 += mat.m00;
		m01 += mat.m01;
		m02 += mat.m02;
		m03 += mat.m03;
		m10 += mat.m10;
		m11 += mat.m11;
		m12 += mat.m12;
		m13 += mat.m13;
		m20 += mat.m20;
		m21 += mat.m21;
		m22 += mat.m22;
		m23 += mat.m23;
		m30 += mat.m30;
		m31 += mat.m31;
		m32 += mat.m32;
		m33 += mat.m33;
	}
	
	public void substract(Mat4f mat)
	{
		m00 -= mat.m00;
		m01 -= mat.m01;
		m02 -= mat.m02;
		m03 -= mat.m03;
		m10 -= mat.m10;
		m11 -= mat.m11;
		m12 -= mat.m12;
		m13 -= mat.m13;
		m20 -= mat.m20;
		m21 -= mat.m21;
		m22 -= mat.m22;
		m23 -= mat.m23;
		m30 -= mat.m30;
		m31 -= mat.m31;
		m32 -= mat.m32;
		m33 -= mat.m33;
	}
	
	public void multiply(Mat4f mat)
	{
		multiply(this, mat, this);
	}
	
	public static void multiply(Mat4f left, Mat4f right, Mat4f destination)
	{
		float m00Temp = left.m00 * right.m00 + left.m10 * right.m01 + left.m20 * right.m02 + left.m30 * right.m03;
		float m01Temp = left.m01 * right.m00 + left.m11 * right.m01 + left.m21 * right.m02 + left.m31 * right.m03;
		float m02Temp = left.m02 * right.m00 + left.m12 * right.m01 + left.m22 * right.m02 + left.m32 * right.m03;
		float m03Temp = left.m03 * right.m00 + left.m13 * right.m01 + left.m23 * right.m02 + left.m33 * right.m03;
		float m10Temp = left.m00 * right.m10 + left.m10 * right.m11 + left.m20 * right.m12 + left.m30 * right.m13;
		float m11Temp = left.m01 * right.m10 + left.m11 * right.m11 + left.m21 * right.m12 + left.m31 * right.m13;
		float m12Temp = left.m02 * right.m10 + left.m12 * right.m11 + left.m22 * right.m12 + left.m32 * right.m13;
		float m13Temp = left.m03 * right.m10 + left.m13 * right.m11 + left.m23 * right.m12 + left.m33 * right.m13;
		float m20Temp = left.m00 * right.m20 + left.m10 * right.m21 + left.m20 * right.m22 + left.m30 * right.m23;
		float m21Temp = left.m01 * right.m20 + left.m11 * right.m21 + left.m21 * right.m22 + left.m31 * right.m23;
		float m22Temp = left.m02 * right.m20 + left.m12 * right.m21 + left.m22 * right.m22 + left.m32 * right.m23;
		float m23Temp = left.m03 * right.m20 + left.m13 * right.m21 + left.m23 * right.m22 + left.m33 * right.m23;
		float m30Temp = left.m00 * right.m30 + left.m10 * right.m31 + left.m20 * right.m32 + left.m30 * right.m33;
		float m31Temp = left.m01 * right.m30 + left.m11 * right.m31 + left.m21 * right.m32 + left.m31 * right.m33;
		float m32Temp = left.m02 * right.m30 + left.m12 * right.m31 + left.m22 * right.m32 + left.m32 * right.m33;
		float m33Temp = left.m03 * right.m30 + left.m13 * right.m31 + left.m23 * right.m32 + left.m33 * right.m33;
		
		destination.m00 = m00Temp;
		destination.m01 = m01Temp;
		destination.m02 = m02Temp;
		destination.m03 = m03Temp;
		destination.m10 = m10Temp;
		destination.m11 = m11Temp;
		destination.m12 = m12Temp;
		destination.m13 = m13Temp;
		destination.m20 = m20Temp;
		destination.m21 = m21Temp;
		destination.m22 = m22Temp;
		destination.m23 = m23Temp;
		destination.m30 = m30Temp;
		destination.m31 = m31Temp;
		destination.m32 = m32Temp;
		destination.m33 = m33Temp;
	}
	
	public static void multiply(Mat4f left, Vec3f right, Vec3f destination)
	{
		float tempX = left.m00 * right.x + left.m01 * right.y + left.m02 * right.z;
		float tempY = left.m10 * right.x + left.m11 * right.y + left.m12 * right.z;
		float tempZ = left.m20 * right.x + left.m21 * right.y + left.m22 * right.z;
		
		destination.x = tempX;
		destination.y = tempY;
		destination.z = tempZ;
	}
	
	public void scale(Vec3f vec)
	{
		m00 *= vec.x;
		m01 *= vec.x;
		m02 *= vec.x;
		m03 *= vec.x;
		m10 *= vec.y;
		m11 *= vec.y;
		m12 *= vec.y;
		m13 *= vec.y;
		m20 *= vec.z;
		m21 *= vec.z;
		m22 *= vec.z;
		m23 *= vec.z;
	}
	
	public void translate(Vec3f vec)
	{
		translate(vec.x, vec.y, vec.z);
	}
	
	public void translate(float x, float y, float z)
	{
		m30 -= m00 * x + m10 * y + m20 * z;
		m31 -= m01 * x + m11 * y + m21 * z;
		m32 -= m02 * x + m12 * y + m22 * z;
		m33 -= m03 * x + m13 * y + m23 * z;
	}
	
	public void rotate(float angle, Vec3f axis)
	{
		float c = (float) Math.cos(angle);
		float s = (float) Math.sin(angle);
		
		float oneminusc = 1.0f - c;
		
		float xy = axis.x * axis.y;
		float yz = axis.y * axis.z;
		float xz = axis.x * axis.z;
		float xs = axis.x * s;
		float ys = axis.y * s;
		float zs = axis.z * s;
		
		float f00 = axis.x * axis.x * oneminusc + c;
		float f01 = xy * oneminusc + zs;
		float f02 = xz * oneminusc - ys;
		float f10 = xy * oneminusc - zs;
		float f11 = axis.y * axis.y * oneminusc + c;
		float f12 = yz * oneminusc + xs;
		float f20 = xz * oneminusc + ys;
		float f21 = yz * oneminusc - xs;
		float f22 = axis.z * axis.z * oneminusc + c;
		
		float t00 = m00 * f00 + m10 * f01 + m20 * f02;
		float t01 = m01 * f00 + m11 * f01 + m21 * f02;
		float t02 = m02 * f00 + m12 * f01 + m22 * f02;
		float t03 = m03 * f00 + m13 * f01 + m23 * f02;
		float t10 = m00 * f10 + m10 * f11 + m20 * f12;
		float t11 = m01 * f10 + m11 * f11 + m21 * f12;
		float t12 = m02 * f10 + m12 * f11 + m22 * f12;
		float t13 = m03 * f10 + m13 * f11 + m23 * f12;
		
		m20 = m00 * f20 + m10 * f21 + m20 * f22;
		m21 = m01 * f20 + m11 * f21 + m21 * f22;
		m22 = m02 * f20 + m12 * f21 + m22 * f22;
		m23 = m03 * f20 + m13 * f21 + m23 * f22;
		
		m00 = t00;
		m01 = t01;
		m02 = t02;
		m03 = t03;
		m10 = t10;
		m11 = t11;
		m12 = t12;
		m13 = t13;
	}
	
	public void store(FloatBuffer buffer)
	{
		buffer.put(m00);
		buffer.put(m01);
		buffer.put(m02);
		buffer.put(m03);
		buffer.put(m10);
		buffer.put(m11);
		buffer.put(m12);
		buffer.put(m13);
		buffer.put(m20);
		buffer.put(m21);
		buffer.put(m22);
		buffer.put(m23);
		buffer.put(m30);
		buffer.put(m31);
		buffer.put(m32);
		buffer.put(m33);
	}
	
	public void negate()
	{
		m00 = -m00;
		m01 = -m01;
		m02 = -m02;
		m03 = -m03;
		m10 = -m10;
		m11 = -m11;
		m12 = -m12;
		m13 = -m13;
		m20 = -m20;
		m21 = -m21;
		m22 = -m22;
		m23 = -m23;
		m30 = -m30;
		m31 = -m31;
		m32 = -m32;
		m33 = -m33;
	}
	
	public void invert()
	{
		float determinant = getDeterminant();
		
		if (determinant != 0)
		{
			float determinantInverse = 1f / determinant;
			
			m00 = getDeterminant3x3(m11, m12, m13, m21, m22, m23, m31, m32, m33) * determinantInverse;
			m11 = -getDeterminant3x3(m10, m12, m13, m20, m22, m23, m30, m32, m33) * determinantInverse;
			m22 = getDeterminant3x3(m10, m11, m13, m20, m21, m23, m30, m31, m33) * determinantInverse;
			m33 = -getDeterminant3x3(m10, m11, m12, m20, m21, m22, m30, m31, m32) * determinantInverse;
			m01 = -getDeterminant3x3(m01, m02, m03, m21, m22, m23, m31, m32, m33) * determinantInverse;
			m10 = getDeterminant3x3(m00, m02, m03, m20, m22, m23, m30, m32, m33) * determinantInverse;
			m20 = -getDeterminant3x3(m00, m01, m03, m20, m21, m23, m30, m31, m33) * determinantInverse;
			m02 = getDeterminant3x3(m00, m01, m02, m20, m21, m22, m30, m31, m32) * determinantInverse;
			m12 = getDeterminant3x3(m01, m02, m03, m11, m12, m13, m31, m32, m33) * determinantInverse;
			m21 = -getDeterminant3x3(m00, m02, m03, m10, m12, m13, m30, m32, m33) * determinantInverse;
			m03 = getDeterminant3x3(m00, m01, m03, m10, m11, m13, m30, m31, m33) * determinantInverse;
			m30 = -getDeterminant3x3(m00, m01, m02, m10, m11, m12, m30, m31, m32) * determinantInverse;
			m13 = -getDeterminant3x3(m01, m02, m03, m11, m12, m13, m21, m22, m23) * determinantInverse;
			m31 = getDeterminant3x3(m00, m02, m03, m10, m12, m13, m20, m22, m23) * determinantInverse;
			m32 = -getDeterminant3x3(m00, m01, m03, m10, m11, m13, m20, m21, m23) * determinantInverse;
			m23 = getDeterminant3x3(m00, m01, m02, m10, m11, m12, m20, m21, m22) * determinantInverse;
		}
	}
	
	public float getDeterminant()
	{
		float f = m00 * ((m11 * m22 * m33 + m12 * m23 * m31 + m13 * m21 * m32) - m13 * m22 * m31 - m11 * m23 * m32 - m12 * m21 * m33);
		f -= m01 * ((m10 * m22 * m33 + m12 * m23 * m30 + m13 * m20 * m32) - m13 * m22 * m30 - m10 * m23 * m32 - m12 * m20 * m33);
		f += m02 * ((m10 * m21 * m33 + m11 * m23 * m30 + m13 * m20 * m31) - m13 * m21 * m30 - m10 * m23 * m31 - m11 * m20 * m33);
		f -= m03 * ((m10 * m21 * m32 + m11 * m22 * m30 + m12 * m20 * m31) - m12 * m21 * m30 - m10 * m22 * m31 - m11 * m20 * m32);
		return f;
	}
	
	private static float getDeterminant3x3(float t00, float t01, float t02, float t10, float t11, float t12, float t20, float t21, float t22)
	{
		return t00 * (t11 * t22 - t12 * t21) + t01 * (t12 * t20 - t10 * t22) + t02 * (t10 * t21 - t11 * t20);
	}
	
	public String toString()
	{
		String st = "Matrix4f";
		st += "\n\t[" + m00 + "][" + m10 + "][" + m20 + "][" + m30 + "]";
		st += "\n\t[" + m01 + "][" + m11 + "][" + m21 + "][" + m31 + "]";
		st += "\n\t[" + m02 + "][" + m12 + "][" + m22 + "][" + m32 + "]";
		st += "\n\t[" + m03 + "][" + m13 + "][" + m23 + "][" + m33 + "]";
		return st;
	}
	
	public void setOrthographicProjection(float right, float left, float top, float bottom, float near, float far)
	{
		setZero();
		
		float width = 1.0f / (right - left);
		float height = 1.0f / (top - bottom);
		float depth = 1.0f / (far - near);
		
		m00 = 2.0f * (width);
		m11 = 2.0f * (height);
		m22 = -2.0f * (depth);
		m30 = -(right + left) * width;
		m31 = -(top + bottom) * height;
		m32 = -(far + near) * depth;
		m33 = 1.0f;
	}
	
	public void lookAt(float eyeX, float eyeY, float eyeZ, float targetX, float targetY, float targetZ)
	{
		setIdentity();
		
		float forwardX = targetX - eyeX;
		float forwardY = targetY - eyeY;
		float forwardZ = targetZ - eyeZ;
		
		float upX = 0;
		float upY = 1;
		float upZ = 0;
		
		if (targetX == eyeX && targetZ == eyeZ && targetY != eyeY)
		{
			upY = 0;
			upZ = 1;
		}
		
		float length = MathHelper.sqrt(forwardX * forwardX + forwardY * forwardY + forwardZ * forwardZ);
		forwardX /= length;
		forwardY /= length;
		forwardZ /= length;
		
		float sideX = (forwardY * upZ) - (forwardZ * upY);
		float sideY = (forwardZ * upX) - (forwardX * upZ);
		float sideZ = (forwardX * upY) - (forwardY * upX);
		
		length = MathHelper.sqrt(sideX * sideX + sideY * sideY + sideZ * sideZ);
		sideX /= length;
		sideY /= length;
		sideZ /= length;
		
		upX = (sideY * forwardZ) - (sideZ * forwardY);
		upY = (sideZ * forwardX) - (sideX * forwardZ);
		upZ = (sideX * forwardY) - (sideY * forwardX);
		
		length = MathHelper.sqrt(upX * upX + upY * upY + upZ * upZ);
		upX /= length;
		upY /= length;
		upZ /= length;
		
		m00 = sideX;
		m10 = sideY;
		m20 = sideZ;
		m01 = upX;
		m11 = upY;
		m21 = upZ;
		m02 = -forwardX;
		m12 = -forwardY;
		m22 = -forwardZ;
		
		translate(eyeX, eyeY, eyeZ);
	}
}
