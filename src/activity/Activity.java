package activity;

public abstract class Activity
{
	public static Activity activityGame;
	public static Activity activityLoading;
	
	private static Activity activeActivity;
	
	public static void createActivities()
	{
		activityGame = new ActivityGame();
		activityLoading = new ActivityLoading();
		
		activeActivity = activityGame;
		activeActivity.unpause();
	}
	
	public static void switchActivity(Activity newActivity)
	{
		if (activeActivity != newActivity)
		{
			activeActivity.pause();
			activeActivity = newActivity;
			activeActivity.unpause();
		}
	}
	
	public static void updateActiveActivity(float delta)
	{
		activeActivity.update(delta);
	}
	
	public static void renderActiveActivity(float delta)
	{
		activeActivity.render(delta);
	}
	
	public static void destroyActivities()
	{
		activityGame.destroy();
		activityLoading.destroy();
	}
	
	public abstract void update(float delta);
	
	public abstract void render(float delta);
	
	public abstract void destroy();
	
	public abstract void pause();
	
	public abstract void unpause();
}