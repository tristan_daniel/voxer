package activity;

import game.block.Block;
import game.player.Player;
import game.shaders.Shaders;
import game.shaders.Sky;
import game.world.BlockManager;
import game.world.World;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

public class ActivityGame extends Activity
{
	public ActivityGame()
	{
		Block.loadBlocks();
		Shaders.init();
		BlockManager.init(64);
		System.gc();
		Sky.init();
	}
	
	@Override
	public void update(float delta)
	{
		
	}
	
	@Override
	public void render(float delta)
	{
		Player.update(delta);
		BlockManager.update();
		World.update(delta);
		
		Shaders.render();
	}
	
	@Override
	public void destroy()
	{
		Shaders.destroy();
	}
	
	@Override
	public void pause()
	{
		Mouse.setGrabbed(false);
		Mouse.setCursorPosition(Display.getWidth() / 2, Display.getHeight() / 2);
	}
	
	@Override
	public void unpause()
	{
		Mouse.setGrabbed(true);
	}
}