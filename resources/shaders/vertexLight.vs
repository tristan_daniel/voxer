#version 150 core

uniform mat4 viewProjectionMatrix;
uniform mat4 viewMatrix;
uniform vec3 lightPosition;

in vec3 in_Position;
in vec3 in_Normal;
in vec2 in_TextureCoord;

out vec3 pass_LightDirection_cameraSpace;
out vec3 pass_Normal_cameraSpace;
out vec3 pass_EyeDirection_cameraSpace;
out vec3 pass_Position;
out vec3 pass_LightPosition_worldSpace;

out vec2 pass_TextureCoord;

void main(void) {
	
	pass_TextureCoord = in_TextureCoord;
	gl_Position = viewProjectionMatrix * vec4(in_Position, 1);
	
	pass_EyeDirection_cameraSpace = - (viewMatrix * vec4(in_Position, 1)).xyz;
	pass_LightDirection_cameraSpace = (viewMatrix * vec4(lightPosition, 1)).xyz + pass_EyeDirection_cameraSpace;
	pass_Normal_cameraSpace = (viewMatrix * vec4(in_Normal, 0)).xyz;
	pass_Position = in_Position;
	pass_LightPosition_worldSpace = lightPosition;
}