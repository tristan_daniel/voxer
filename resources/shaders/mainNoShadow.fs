#version 150 core

uniform sampler2DArray diffuseTextures;

in vec3 pass_TextureCoord;
in float pass_Lighting;

out vec4 out_Color;

void main(void)
{
	out_Color = texture(diffuseTextures, pass_TextureCoord);

	out_Color.rgb *= pass_Lighting;
}