#version 150 core

uniform mat4 viewProjectionMatrix;
uniform mat4 depthViewProjectionMatrix;

in vec3 in_Position;
in vec3 in_TextureCoord;
in float in_Lighting;

out vec3 pass_TextureCoord;
out vec4 pass_ShadowCoord;
out float pass_Lighting;

const mat4 bias = mat4(	0.5, 0.0, 0.0, 0.0,
						0.0, 0.5, 0.0, 0.0,
						0.0, 0.0, 0.5, 0.0,
						0.5, 0.5, 0.5, 1.0);

void main(void)
{
	gl_Position = viewProjectionMatrix * vec4(in_Position, 1.0);
	pass_TextureCoord = in_TextureCoord;
	pass_ShadowCoord = bias * depthViewProjectionMatrix * vec4(in_Position, 1.0);
	pass_Lighting = in_Lighting;
}