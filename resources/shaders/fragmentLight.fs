#version 150 core

uniform sampler2D texture_diffuse;

in vec3 pass_EyeDirection_cameraSpace;
in vec3 pass_LightDirection_cameraSpace;
in vec3 pass_Normal_cameraSpace;
in vec3 pass_Position;
in vec3 pass_LightPosition_worldSpace;

in vec2 pass_TextureCoord;

out vec4 out_Color;

void main(void) {
	vec3 n = normalize(pass_Normal_cameraSpace);
	vec3 l = normalize(pass_LightDirection_cameraSpace);
	vec3 E = normalize(pass_EyeDirection_cameraSpace);
	vec3 R = reflect(-l, n);
	
	float cosTheta = clamp(dot(n, l), 0, 1);
	float cosAlpha = clamp(dot(E, R), 0, 1);
	
	float distanceToLightSquare = pow(pass_Position.x - pass_LightPosition_worldSpace.x, 2) + pow(pass_Position.y - pass_LightPosition_worldSpace.y, 2) + pow(pass_Position.z - pass_LightPosition_worldSpace.z, 2);
	//distanceToLightSquare = 1;
	float lightPower = 500;
	
	vec4 diffuse = vec4(0.8, 0.8, 0.8, 1);
	diffuse = texture(texture_diffuse, pass_TextureCoord);
	float specular = 0;
	
	out_Color = diffuse + diffuse * lightPower * cosTheta / distanceToLightSquare + specular * lightPower * pow(cosAlpha, 5) / distanceToLightSquare;
}
