#version 150 core

uniform mat4 viewProjectionMatrix;

in vec3 in_Position;

void main(void)
{
	gl_Position = viewProjectionMatrix * vec4(in_Position, 1.0);
}