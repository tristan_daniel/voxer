#version 150 core

uniform sampler2DArray diffuseTextures;
uniform sampler2DShadow shadowMap;

in vec3 pass_TextureCoord;
in vec4 pass_ShadowCoord;
in float pass_Lighting;

out vec4 out_Color;

void main(void)
{
	out_Color = texture(diffuseTextures, pass_TextureCoord);
	
	float shadowFactor = 1.0;
	vec4 shadowCoordPostW = pass_ShadowCoord / pass_ShadowCoord.w;
	
	if (pass_ShadowCoord.w > 0.0f && shadowCoordPostW.x > 0 && shadowCoordPostW.y > 0 && shadowCoordPostW.x < 1 && shadowCoordPostW.y < 1)
	{
		float shadow = 0.0;
		shadow += textureProjOffset(shadowMap, pass_ShadowCoord, ivec2(-1, 1));
		shadow += textureProjOffset(shadowMap, pass_ShadowCoord, ivec2( 1, 1));
		shadow += textureProjOffset(shadowMap, pass_ShadowCoord, ivec2(-1, -1));
		shadow += textureProjOffset(shadowMap, pass_ShadowCoord, ivec2( 1, -1));
		if(shadow < 4.0)
			shadowFactor = 0.5;
	}

	out_Color.rgb *= shadowFactor * pass_Lighting;
}