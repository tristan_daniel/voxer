#version 150 core

uniform mat4 viewProjectionMatrix;

in vec3 in_Position;

out vec3 pass_Position;

void main(void) 
{
    luminance = 1;
    gl_Position = viewProjectionMatrix * vec4(in_Position, 1.0);
    pass_Position = in_Position;
}