#version 150 core

uniform mat4 viewProjectionMatrix;

in vec3 in_Position;
in vec3 in_TextureCoord;
in float in_Lighting;

out vec3 pass_TextureCoord;
out float pass_Lighting;

void main(void)
{
	gl_Position = viewProjectionMatrix * vec4(in_Position, 1.0);
	pass_TextureCoord = in_TextureCoord;
	pass_Lighting = in_Lighting;
}